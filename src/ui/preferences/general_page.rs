use super::PreferencesWindow;
use crate::settings::SettingsKey;
use crate::utils::native_file_chooser_async;
use adw::prelude::*;
use adw::subclass::prelude::*;
use gettextrs::gettext;
use glib::WeakRef;
use gtk::{FileChooserAction, FileFilter};
use once_cell::sync::OnceCell;

mod imp {
    use super::*;
    use gtk::CompositeTemplate;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/stefan99353/cobble/ui/preferences/general_page.ui")]
    pub struct GeneralPage {
        #[template_child]
        pub parallel_downloads_spinbutton: TemplateChild<gtk::SpinButton>,
        #[template_child]
        pub download_retries_spinbutton: TemplateChild<gtk::SpinButton>,
        #[template_child]
        pub verify_downloads_switch: TemplateChild<gtk::Switch>,
        #[template_child]
        pub default_instances_path_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub default_instances_path_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub default_libraries_path_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub default_libraries_path_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub default_assets_path_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub default_assets_path_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub instance_index_path_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub instance_index_path_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub profile_index_path_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub profile_index_path_label: TemplateChild<gtk::Label>,

        pub window: OnceCell<WeakRef<PreferencesWindow>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for GeneralPage {
        const NAME: &'static str = "GeneralPage";
        type Type = super::GeneralPage;
        type ParentType = adw::PreferencesPage;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_instance_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for GeneralPage {
        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.instance();

            obj.setup_widgets();
        }
    }

    impl WidgetImpl for GeneralPage {}
    impl PreferencesPageImpl for GeneralPage {}
}

glib::wrapper! {
    pub struct GeneralPage(ObjectSubclass<imp::GeneralPage>)
    @extends gtk::Widget, adw::PreferencesPage;
}

#[gtk::template_callbacks]
impl GeneralPage {
    fn setup_widgets(&self) {
        let imp = self.imp();

        SettingsKey::ParallelDownloads.bind_property(&*imp.parallel_downloads_spinbutton, "value");
        SettingsKey::DownloadRetries.bind_property(&*imp.download_retries_spinbutton, "value");
        SettingsKey::VerifyDownloads.bind_property(&*imp.verify_downloads_switch, "state");

        SettingsKey::DefaultInstancesPath
            .bind_property(&*imp.default_instances_path_label, "label");
        SettingsKey::DefaultLibrariesPath
            .bind_property(&*imp.default_libraries_path_label, "label");
        SettingsKey::DefaultAssetsPath.bind_property(&*imp.default_assets_path_label, "label");
        SettingsKey::InstanceIndexPath.bind_property(&*imp.instance_index_path_label, "label");
        SettingsKey::ProfileIndexPath.bind_property(&*imp.profile_index_path_label, "label");
    }

    #[template_callback]
    #[instrument(name = "on_instance_path_clicked", skip_all, fields(path, setting))]
    async fn on_instance_path_clicked(&self) {
        trace!("Opening file chooser dialog");
        self.file_chooser(
            SettingsKey::DefaultInstancesPath,
            &gettext("Select Default Instance Location"),
            FileChooserAction::SelectFolder,
            &[],
        )
        .await;
    }

    #[template_callback]
    #[instrument(name = "on_libraries_path_clicked", skip_all, fields(path, setting))]
    async fn on_libraries_path_clicked(&self) {
        trace!("Opening file chooser dialog");
        self.file_chooser(
            SettingsKey::DefaultLibrariesPath,
            &gettext("Select Default Libraries Location"),
            FileChooserAction::SelectFolder,
            &[],
        )
        .await;
    }

    #[template_callback]
    #[instrument(name = "on_assets_path_clicked", skip_all, fields(path, setting))]
    async fn on_assets_path_clicked(&self) {
        trace!("Opening file chooser dialog");
        self.file_chooser(
            SettingsKey::DefaultAssetsPath,
            &gettext("Select Default Assets Location"),
            FileChooserAction::SelectFolder,
            &[],
        )
        .await;
    }

    #[template_callback]
    #[instrument(
        name = "on_instance_index_path_clicked",
        skip_all,
        fields(path, setting)
    )]
    async fn on_instance_index_path_clicked(&self) {
        let filter = FileFilter::new();
        filter.add_mime_type("application/json");
        filter.set_name(Some(&gettext("JSON")));

        trace!("Opening file chooser dialog");
        self.file_chooser(
            SettingsKey::InstanceIndexPath,
            &gettext("Select Instance Index File"),
            FileChooserAction::Save,
            &[filter],
        )
        .await;
    }

    #[template_callback]
    #[instrument(name = "on_profile_index_clicked", skip_all, fields(path, setting))]
    async fn on_profile_index_clicked(&self) {
        let filter = FileFilter::new();
        filter.add_mime_type("application/json");
        filter.set_name(Some(&gettext("JSON")));

        trace!("Opening file chooser dialog");
        self.file_chooser(
            SettingsKey::ProfileIndexPath,
            &gettext("Select Profile Index File"),
            FileChooserAction::Save,
            &[filter],
        )
        .await;
    }

    async fn file_chooser(
        &self,
        key: SettingsKey,
        title: &str,
        action: FileChooserAction,
        filters: &[FileFilter],
    ) {
        tracing::Span::current().record("setting", key.name());

        let parent = self.imp().window.get().and_then(|p| p.upgrade());
        let path = native_file_chooser_async(
            Some(title),
            parent.as_ref(),
            action,
            Some(&gettext("Select")),
            Some(&gettext("Cancel")),
            filters,
            true,
        )
        .await;

        if let Some(path) = path {
            tracing::Span::current().record("path", tracing::field::display(path.display()));

            trace!("Converting path to a unicode string");
            match path.to_str() {
                Some(path) => {
                    trace!("Saving path to settings");
                    key.set_string(path);
                }
                None => {
                    error!("Path is not valid unicode");
                }
            }
        }
    }

    pub fn set_window(&self, window: WeakRef<PreferencesWindow>) {
        let imp = self.imp();

        imp.window.set(window).unwrap();
    }
}
