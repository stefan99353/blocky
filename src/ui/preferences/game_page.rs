use super::PreferencesWindow;
use crate::settings::SettingsKey;
use crate::utils::native_file_chooser_async;
use adw::prelude::*;
use adw::subclass::prelude::*;
use gettextrs::gettext;
use glib::WeakRef;
use gtk::FileChooserAction;
use once_cell::sync::OnceCell;

mod imp {
    use super::*;
    use gtk::CompositeTemplate;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/stefan99353/cobble/ui/preferences/game_page.ui")]
    pub struct GamePage {
        #[template_child]
        pub enable_global_fullscreen_switch: TemplateChild<gtk::Switch>,
        #[template_child]
        pub enable_global_window_size_expander: TemplateChild<adw::ExpanderRow>,
        #[template_child]
        pub global_window_width_spinbutton: TemplateChild<gtk::SpinButton>,
        #[template_child]
        pub global_window_height_spinbutton: TemplateChild<gtk::SpinButton>,
        #[template_child]
        pub enable_global_memory_expander: TemplateChild<adw::ExpanderRow>,
        #[template_child]
        pub global_min_memory_spinbutton: TemplateChild<gtk::SpinButton>,
        #[template_child]
        pub global_max_memory_spinbutton: TemplateChild<gtk::SpinButton>,
        #[template_child]
        pub global_java_exec_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub global_java_exec_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub enable_global_jvm_args_expander: TemplateChild<adw::ExpanderRow>,
        #[template_child]
        pub global_jvm_args_entryrow: TemplateChild<adw::EntryRow>,

        pub window: OnceCell<WeakRef<PreferencesWindow>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for GamePage {
        const NAME: &'static str = "GamePage";
        type Type = super::GamePage;
        type ParentType = adw::PreferencesPage;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_instance_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for GamePage {
        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.instance();

            obj.setup_widgets();
        }
    }

    impl WidgetImpl for GamePage {}
    impl PreferencesPageImpl for GamePage {}
}

glib::wrapper! {
    pub struct GamePage(ObjectSubclass<imp::GamePage>)
    @extends gtk::Widget, adw::PreferencesPage;
}

#[gtk::template_callbacks]
impl GamePage {
    fn setup_widgets(&self) {
        let imp = self.imp();

        SettingsKey::EnableGlobalFullscreen
            .bind_property(&*imp.enable_global_fullscreen_switch, "state");
        SettingsKey::EnableGlobalWindowSize
            .bind_property(&*imp.enable_global_window_size_expander, "enable-expansion");
        SettingsKey::GlobalWindowWidth.bind_property(&*imp.global_window_width_spinbutton, "value");
        SettingsKey::GlobalWindowHeight
            .bind_property(&*imp.global_window_height_spinbutton, "value");
        SettingsKey::EnableGlobalMemory
            .bind_property(&*imp.enable_global_memory_expander, "enable-expansion");
        SettingsKey::GlobalMinMemory.bind_property(&*imp.global_min_memory_spinbutton, "value");
        SettingsKey::GlobalMaxMemory.bind_property(&*imp.global_max_memory_spinbutton, "value");
        SettingsKey::GlobalJavaExec.bind_property(&*imp.global_java_exec_label, "label");
        SettingsKey::EnableGlobalJvmArgs
            .bind_property(&*imp.enable_global_jvm_args_expander, "enable-expansion");
        SettingsKey::GlobalJvmArgs.bind_property(&*imp.global_jvm_args_entryrow, "text");
    }

    #[template_callback]
    #[instrument(name = "on_java_exec_clicked", skip_all, fields(path))]
    async fn on_java_exec_clicked(&self) {
        let parent = self.imp().window.get().and_then(|p| p.upgrade());

        trace!("Opening file chooser dialog");
        let path = native_file_chooser_async(
            Some(&gettext("Select Java Executable")),
            parent.as_ref(),
            FileChooserAction::Open,
            Some(&gettext("Select")),
            Some(&gettext("Cancel")),
            &[],
            true,
        )
        .await;

        if let Some(path) = path {
            tracing::Span::current().record("path", tracing::field::display(path.display()));

            trace!("Converting path to a unicode string");
            match path.to_str() {
                Some(path) => {
                    trace!("Saving path to settings");
                    SettingsKey::GlobalJavaExec.set_string(path);
                }
                None => {
                    error!("Path is not valid unicode");
                }
            }
        }
    }

    pub fn set_window(&self, window: WeakRef<PreferencesWindow>) {
        let imp = self.imp();

        imp.window.set(window).unwrap();
    }
}
