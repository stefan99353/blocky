use super::{GamePage, GeneralPage, ProfilesPage};
use crate::config;
use adw::prelude::*;
use adw::subclass::prelude::*;

mod imp {
    use super::*;
    use gtk::CompositeTemplate;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/stefan99353/cobble/ui/preferences/window.ui")]
    pub struct PreferencesWindow {
        #[template_child]
        pub general_page: TemplateChild<GeneralPage>,
        #[template_child]
        pub game_page: TemplateChild<GamePage>,
        #[template_child]
        pub profiles_page: TemplateChild<ProfilesPage>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for PreferencesWindow {
        const NAME: &'static str = "PreferencesWindow";
        type Type = super::PreferencesWindow;
        type ParentType = adw::PreferencesWindow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for PreferencesWindow {
        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.instance();

            if config::PROFILE == "Devel" {
                obj.add_css_class("devel");
            }

            obj.setup_widgets();
        }
    }

    impl WidgetImpl for PreferencesWindow {}
    impl WindowImpl for PreferencesWindow {}
    impl AdwWindowImpl for PreferencesWindow {}
    impl PreferencesWindowImpl for PreferencesWindow {}
}

glib::wrapper! {
    pub struct PreferencesWindow(ObjectSubclass<imp::PreferencesWindow>)
    @extends gtk::Widget, gtk::Window, adw::Window, adw::PreferencesWindow;
}

impl PreferencesWindow {
    pub fn new() -> Self {
        glib::Object::new::<Self>(&[])
    }

    fn setup_widgets(&self) {
        let imp = self.imp();

        imp.general_page.set_window(self.downgrade());
        imp.game_page.set_window(self.downgrade());
        imp.profiles_page.set_window(self.downgrade());
    }
}

impl Default for PreferencesWindow {
    fn default() -> Self {
        Self::new()
    }
}
