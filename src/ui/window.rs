use crate::application::Application;
use crate::config;
use crate::gobject::{GCobbleProfile, GProfileManager};
use crate::settings::SettingsKey;
use crate::ui::ContentBox;
use adw::prelude::*;
use adw::subclass::prelude::*;
use gtk::SignalListItemFactory;

mod imp {
    use super::*;
    use gtk::CompositeTemplate;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/stefan99353/cobble/ui/window.ui")]
    pub struct Window {
        #[template_child]
        pub profile_dropdown: TemplateChild<gtk::DropDown>,
        #[template_child]
        pub toast_overlay: TemplateChild<adw::ToastOverlay>,
        #[template_child]
        pub content_box: TemplateChild<ContentBox>,

        pub settings: gio::Settings,
    }

    impl Default for Window {
        fn default() -> Self {
            Self {
                profile_dropdown: TemplateChild::default(),
                toast_overlay: TemplateChild::default(),
                content_box: TemplateChild::default(),
                settings: gio::Settings::new(config::APP_ID),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Window {
        const NAME: &'static str = "Window";
        type Type = super::Window;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_instance_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for Window {
        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.instance();

            if config::PROFILE == "Devel" {
                obj.add_css_class("devel");
            }

            // Load latest window state
            obj.load_window_state();

            obj.setup_widgets();
        }
    }

    impl WidgetImpl for Window {}
    impl WindowImpl for Window {
        fn close_request(&self) -> gtk::Inhibit {
            if let Err(err) = self.instance().save_window_state() {
                warn!("Failed to save window state: {}", &err);
            }

            self.parent_close_request()
        }
    }

    impl ApplicationWindowImpl for Window {}
    impl AdwApplicationWindowImpl for Window {}
}

glib::wrapper! {
    pub struct Window(ObjectSubclass<imp::Window>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, adw::ApplicationWindow,
        @implements gio::ActionMap, gio::ActionGroup, gtk::Native, gtk::Root;
}

#[gtk::template_callbacks]
impl Window {
    pub fn new(app: &Application) -> Self {
        glib::Object::new(&[("application", app)])
    }

    fn setup_widgets(&self) {
        let imp = self.imp();
        let profile_manager = GProfileManager::default();

        // Set profiles
        let profiles = profile_manager.profiles();
        imp.profile_dropdown.set_factory(Some(&list_factory()));
        imp.profile_dropdown.set_model(Some(&profiles));
    }

    #[template_callback]
    #[instrument(name = "on_profile_changed", skip_all)]
    fn on_profile_changed(&self) {
        let imp = self.imp();
        let profile_manager = GProfileManager::default();

        trace!("Getting selected profile");
        let profile = imp
            .profile_dropdown
            .selected_item()
            .map(|p| p.downcast::<GCobbleProfile>().unwrap());

        trace!("Setting profile");
        profile_manager.set_current_profile(profile);
    }

    #[instrument(name = "save_window_state", skip_all, fields(width, height, maximized))]
    fn save_window_state(&self) -> Result<(), glib::BoolError> {
        let imp = self.imp();

        trace!("Getting current window state");
        let (width, height) = self.default_size();
        let maximized = self.is_maximized();

        tracing::Span::current().record("width", width);
        tracing::Span::current().record("height", height);
        tracing::Span::current().record("maximized", maximized);

        trace!("Saving window width");
        imp.settings
            .set_int(SettingsKey::WindowWidth.name(), width)?;
        trace!("Saving window height");
        imp.settings
            .set_int(SettingsKey::WindowHeight.name(), height)?;
        trace!("Saving window maximized");
        imp.settings
            .set_boolean(SettingsKey::WindowMaximized.name(), maximized)?;

        Ok(())
    }

    #[instrument(name = "load_window_state", skip_all, fields(width, height, maximized))]
    fn load_window_state(&self) {
        let imp = self.imp();

        let width = imp.settings.int(SettingsKey::WindowWidth.name());
        let height = imp.settings.int(SettingsKey::WindowHeight.name());
        let maximized = imp.settings.boolean(SettingsKey::WindowMaximized.name());

        tracing::Span::current().record("width", width);
        tracing::Span::current().record("height", height);
        tracing::Span::current().record("maximized", maximized);

        trace!("Setting window dimensions");
        self.set_default_size(width, height);

        if maximized {
            trace!("Maximizing window");
            self.maximize();
        }
    }
}

fn list_factory() -> SignalListItemFactory {
    let factory = SignalListItemFactory::new();

    factory.connect_bind(move |_, item| {
        let item = item.clone().downcast::<gtk::ListItem>().unwrap();
        let profile = item.item().unwrap().downcast::<GCobbleProfile>().unwrap();

        let label = gtk::Label::new(Some(&profile.player_name()));
        label.set_valign(gtk::Align::Center);
        label.set_halign(gtk::Align::Start);
        item.set_child(Some(&label));
    });

    factory
}
