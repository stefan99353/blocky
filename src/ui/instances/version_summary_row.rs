use crate::gobject::GVersionSummary;
use adw::prelude::*;
use adw::subclass::prelude::*;
use glib::{ParamFlags, ParamSpec, ParamSpecObject};
use once_cell::sync::Lazy;
use once_cell::sync::OnceCell;
use time::format_description;

mod imp {
    use super::*;
    use gtk::CompositeTemplate;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/stefan99353/cobble/ui/instances/version_summary_row.ui")]
    pub struct VersionSummaryRow {
        #[template_child]
        pub id_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub type_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub release_time_label: TemplateChild<gtk::Label>,

        pub version_summary: OnceCell<GVersionSummary>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for VersionSummaryRow {
        const NAME: &'static str = "VersionSummaryRow";
        type Type = super::VersionSummaryRow;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for VersionSummaryRow {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![ParamSpecObject::new(
                    super::VersionSummaryRow::VERSION_SUMMARY,
                    "Version Summary",
                    "Version Summary",
                    GVersionSummary::static_type(),
                    ParamFlags::READWRITE | ParamFlags::CONSTRUCT_ONLY,
                )]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &glib::Value, pspec: &glib::ParamSpec) {
            match pspec.name() {
                super::VersionSummaryRow::VERSION_SUMMARY => {
                    self.version_summary.set(value.get().unwrap()).unwrap()
                }
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn property(&self, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
            match pspec.name() {
                super::VersionSummaryRow::VERSION_SUMMARY => self.version_summary.get().to_value(),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.instance();

            obj.setup_widgets();
        }
    }

    impl WidgetImpl for VersionSummaryRow {}
    impl BoxImpl for VersionSummaryRow {}
}

glib::wrapper! {
    pub struct VersionSummaryRow(ObjectSubclass<imp::VersionSummaryRow>)
    @extends gtk::Widget, gtk::Box;
}

impl VersionSummaryRow {
    pub const VERSION_SUMMARY: &str = "version-summary";

    pub fn new(summary: &GVersionSummary) -> Self {
        glib::Object::new(&[(Self::VERSION_SUMMARY, summary)])
    }

    fn setup_widgets(&self) {
        let imp = self.imp();
        let version = self.version_summary();

        imp.id_label.set_label(&version.id());
        imp.type_label.set_label(&version.type_().to_string());
        let format = format_description::parse("[year]-[month]-[day] [hour]:[minute]").unwrap();
        imp.release_time_label
            .set_label(&version.release_time().format(&format).unwrap());
    }

    fn version_summary(&self) -> GVersionSummary {
        self.property(Self::VERSION_SUMMARY)
    }
}
