use super::properties::InstancePropertiesWindow;
use crate::application::Application;
use crate::config;
use crate::gobject::{GInstance, GInstanceManager, GProfileManager};
use crate::settings::SettingsKey;
use crate::utils::{error_dialog, refresh_and_save_profile, save_instance, spawn_tokio_future};
use adw::prelude::*;
use adw::subclass::prelude::*;
use cobble_core::minecraft::{
    AssetInstallationUpdate, ClientInstallationUpdate, GameProcess, GameProcessHandle,
    InstallationUpdate, LaunchOptions, LaunchOptionsBuilder, LibraryInstallationUpdate,
    LogConfigInstallationUpdate,
};
use cobble_core::profile::CobbleProfile;
use cobble_core::Instance;
use futures::future::join;
use futures::stream::{AbortHandle, Abortable};
use gettextrs::gettext;
use glib::clone;
use glib::{ParamFlags, ParamSpec, ParamSpecObject};
use gtk_macros::action;
use once_cell::sync::Lazy;
use once_cell::sync::OnceCell;
use std::cell::RefCell;
use std::process::Stdio;
use tokio::sync::mpsc::Receiver;

mod imp {
    use super::*;
    use gtk::CompositeTemplate;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/stefan99353/cobble/ui/instances/instance_row.ui")]
    pub struct InstanceRow {
        #[template_child]
        pub name_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub description_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub version_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub button_stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub launch_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub stop_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub install_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub cancel_install_button: TemplateChild<gtk::Button>,

        #[template_child]
        pub progress_box: TemplateChild<gtk::Box>,
        #[template_child]
        pub progress_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub progress_bar: TemplateChild<gtk::ProgressBar>,

        pub popover_menu: OnceCell<gtk::PopoverMenu>,
        pub instance: OnceCell<GInstance>,

        pub install_abort_handle: RefCell<Option<AbortHandle>>,
        pub game_handle: RefCell<Option<GameProcessHandle>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for InstanceRow {
        const NAME: &'static str = "InstanceRow";
        type Type = super::InstanceRow;
        type ParentType = gtk::ListBoxRow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for InstanceRow {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![ParamSpecObject::new(
                    super::InstanceRow::INSTANCE,
                    "Instance",
                    "Instance",
                    GInstance::static_type(),
                    ParamFlags::READWRITE | ParamFlags::CONSTRUCT_ONLY,
                )]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &glib::Value, pspec: &ParamSpec) {
            match pspec.name() {
                super::InstanceRow::INSTANCE => self.instance.set(value.get().unwrap()).unwrap(),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
            match pspec.name() {
                super::InstanceRow::INSTANCE => self.instance.get().to_value(),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.instance();

            obj.setup_widgets();
            obj.setup_actions();
            obj.setup_signals();
        }

        fn dispose(&self) {
            self.popover_menu.get().unwrap().unparent();
        }
    }

    impl WidgetImpl for InstanceRow {}
    impl ListBoxRowImpl for InstanceRow {}
}

glib::wrapper! {
    pub struct InstanceRow(ObjectSubclass<imp::InstanceRow>)
    @extends gtk::Widget, gtk::ListBoxRow;
}

impl InstanceRow {
    pub const INSTANCE: &str = "instance";

    pub fn new(instance: &GInstance) -> Self {
        glib::Object::new(&[(Self::INSTANCE, instance)])
    }

    fn setup_widgets(&self) {
        let imp = self.imp();

        self.bind_property("name", &imp.name_label.get(), "label");
        self.bind_property("description", &imp.description_label.get(), "label");
        self.bind_property("version", &imp.version_label.get(), "label");

        // Popover
        let builder = gtk::Builder::from_resource(
            "/com/gitlab/stefan99353/cobble/ui/instances/instance_menu.ui",
        );
        let menu: gio::MenuModel = builder.object("instance_menu").unwrap();
        let popover_menu = gtk::PopoverMenu::from_model(Some(&menu));
        popover_menu.set_parent(self);
        popover_menu.set_has_arrow(true);
        imp.popover_menu.set(popover_menu).unwrap();

        // Check installed state
        match self.instance().installed() {
            true => self.set_button_view(ButtonView::Launch),
            false => self.set_button_view(ButtonView::Install),
        }
    }

    fn setup_signals(&self) {
        // Popover menu
        let controller = gtk::GestureClick::new();
        controller.set_button(gdk::BUTTON_SECONDARY);
        controller.connect_pressed(clone!(@weak self as this => move |c, _, x, y| {
            this.show_popover_menu(Some(c), x, y);
        }));
        self.add_controller(&controller);
    }

    fn show_popover_menu<G>(&self, controller: Option<&G>, x: f64, y: f64)
    where
        G: IsA<gtk::Gesture>,
    {
        if let Some(controller) = controller {
            controller.set_state(gtk::EventSequenceState::Claimed);
        }

        let coordinates = gdk::Rectangle::new(x as i32, y as i32, 0, 0);

        let imp = self.imp();
        let menu = imp.popover_menu.get().unwrap();
        menu.set_pointing_to(Some(&coordinates));
        menu.popup();
    }

    #[instrument(name = "setup_actions", skip_all, fields(instance = %self.instance().uuid()))]
    fn setup_actions(&self) {
        trace!("Setting up actions for instance");

        let actions = gio::SimpleActionGroup::new();
        self.insert_action_group("instance", Some(&actions));

        trace!("Adding `instance.launch`");
        action!(
            actions,
            "launch",
            clone!(@weak self as this => move |_, _| this.launch())
        );

        trace!("Adding `instance.stop`");
        action!(
            actions,
            "stop",
            clone!(@weak self as this => move |_, _| this.stop())
        );

        trace!("Adding `instance.install`");
        action!(
            actions,
            "install",
            clone!(@weak self as this => move |_, _| this.setup_install())
        );

        trace!("Adding `instance.cancel-install`");
        action!(
            actions,
            "cancel-install",
            clone!(@weak self as this => move |_, _| this.cancel_install())
        );

        trace!("Adding `instance.edit`");
        action!(
            actions,
            "edit",
            clone!(@weak self as this => move |_, _| this.edit())
        );

        trace!("Adding `instance.remove`");
        action!(
            actions,
            "remove",
            clone!(@weak self as this => move |_, _| this.ask_remove())
        );
    }

    #[instrument(name = "launch", skip_all, fields(instance = %self.instance().uuid()))]
    fn launch(&self) {
        debug!("Launching instance");

        let application = Application::default();
        application.toast_notification(&gettext("Launching instance..."));

        let ctx = glib::MainContext::default();
        ctx.spawn_local(clone!(@weak self as this => async move {
            let instance = Instance::from(this.instance());

            trace!("Getting the current profile");
            let profile = GProfileManager::default().current_profile().map(CobbleProfile::from);

            // Update UI
            this.set_button_view(ButtonView::Stop);

            trace!("Refreshing the profile");
            let profile = spawn_tokio_future(async move {
                let mut profile = profile;
                if let Some(profile) = &mut profile {
                    refresh_and_save_profile(profile).await;
                }
                profile
            }).await;
            // TODO: Updating profile in GProfileManager

            trace!("Building launch options");
            let options = build_launch_options(&instance, profile);
            trace!("Starting the detached launch");
            let game_handle_result = spawn_tokio_future(async move {
                instance.detached_launch(&options, Stdio::null(), Stdio::null(), Stdio::null()).await
            }).await;

            match game_handle_result {
                Ok(handle) => {
                    trace!("Game process launched/forked successfully");
                    *this.imp().game_handle.borrow_mut() = Some(handle);
                    this.wait_stopped().await;
                },
                Err(err) => {
                    error_dialog(err);
                    this.set_button_view(ButtonView::Launch);
                },
            }
        }));
    }

    #[instrument(name = "wait_stopped", skip_all)]
    async fn wait_stopped(&self) {
        trace!("Getting game handle");
        let game_handle = self.imp().game_handle.borrow().clone();

        match game_handle {
            Some(game_handle) => {
                trace!("Wait for the game to process to finish");
                let wait_result = spawn_tokio_future(async move { game_handle.wait().await }).await;
                debug!("Instance exited");

                if let Err(err) = wait_result {
                    error_dialog(err);
                }

                // Update UI
                self.set_button_view(ButtonView::Launch);
            }
            None => {
                warn!("There is no game handle set. How did that happen...");
            }
        }
    }

    #[instrument(name = "stop", skip_all, fields(instance = %self.instance().uuid()))]
    fn stop(&self) {
        debug!("Stopping instance");

        let application = Application::default();
        application.toast_notification(&gettext("Stopping instance..."));

        let ctx = glib::MainContext::default();
        ctx.spawn_local(clone!(@weak self as this => async move {
            trace!("Getting game handle");
            let game_handle = this.imp().game_handle.borrow().clone();

            match game_handle {
                Some(game_handle) => {
                    trace!("Sending the shutdown signal to the process. The `wait_stopped` reaps the process and updates the UI");
                    let stop_result = spawn_tokio_future(async move {game_handle.stop().await}).await;

                    if let Err(err) = stop_result {
                        error_dialog(err);
                    }
                }
                None => {
                    warn!("There is no game handle set. How did that happen...");
                }
            }
        }));
    }

    #[instrument(name = "setup_install", skip_all, fields(instance = %self.instance().uuid()))]
    fn setup_install(&self) {
        debug!("Installing instance");

        let ctx = glib::MainContext::default();
        ctx.spawn_local(clone!(@weak self as this => async move {
            let imp = this.imp();

            trace!("Updating UI to show progress");
            imp.progress_box.set_visible(true);
            imp.progress_label.set_label(&gettext("Preparing installation..."));
            this.set_button_view(ButtonView::CancelInstall);

            // Start installation future
            let (abort_handle, abort_registration) = AbortHandle::new_pair();
            let install_future = Abortable::new(this.install(), abort_registration);
            *imp.install_abort_handle.borrow_mut() = Some(abort_handle);

            trace!("Starting installation process");
            match install_future.await {
                Ok(_) => {
                    trace!("Installation finished");

                    trace!("Updating UI to show finished installation");
                    imp.progress_box.set_visible(false);
                    this.set_button_view(ButtonView::Launch);

                    trace!("Updating instances installed state");
                    let instance = this.instance();
                    instance.set_installed(true);
                    spawn_tokio_future(save_instance(Instance::from(instance))).await;
                },
                Err(_) => {
                    trace!("Installation process aborted");

                    trace!("Updating UI");
                    imp.progress_box.set_visible(false);
                    match this.instance().installed() {
                        true => this.set_button_view(ButtonView::Launch),
                        false => this.set_button_view(ButtonView::Install),
                    }
                },
            }
        }));
    }

    #[instrument(name = "install", skip_all)]
    async fn install(&self) {
        let mut instance = Instance::from(self.instance());

        trace!("Creating channel for processing updates");
        let (update_sender, update_receiver) = InstallationUpdate::channel(500);

        trace!("Setting up futures for installation and updating the UI");
        let install_future = spawn_tokio_future(async move {
            instance.full_installation(5, 5, true, update_sender).await
        });
        let update_future = self.process_install_update(update_receiver);

        trace!("Performing installation");
        let (install_result, _) = join(install_future, update_future).await;

        if let Err(err) = install_result {
            error_dialog(err);
        }
    }

    #[instrument(name = "process_install_update", skip_all, fields(instance = %self.instance().uuid()))]
    async fn process_install_update(&self, mut rx: Receiver<InstallationUpdate>) {
        let mut processed_libraries: Vec<String> = vec![];
        let mut processed_assets: Vec<String> = vec![];

        while let Some(update) = rx.recv().await {
            trace!("Received installation progress update");

            match update {
                InstallationUpdate::Library((name, progress)) => {
                    self.process_library_install_update(name, progress, &mut processed_libraries);
                }
                InstallationUpdate::Asset((name, progress)) => {
                    self.process_asset_install_update(name, progress, &mut processed_assets);
                }
                InstallationUpdate::LogConfig(progress) => {
                    self.process_log_config_install_update(progress);
                }
                InstallationUpdate::Client(progress) => {
                    self.process_client_install_update(progress);
                }
            };
        }
    }

    #[instrument(
        name = "process_library_install_update",
        skip_all,
        fields(
            library_name = name,
            total_progress,
            library_progress
        )
    )]
    fn process_library_install_update(
        &self,
        name: String,
        progress: LibraryInstallationUpdate,
        processed_libraries: &mut Vec<String>,
    ) {
        match progress {
            LibraryInstallationUpdate::Downloading(progress) => {
                trace!("Update is a download update");

                if processed_libraries.len() > progress.total_files {
                    debug!("Clearing processed items. Probably new libraries are downloaded.");
                    processed_libraries.clear();
                }

                let mut first_update = false;
                if !processed_libraries.contains(&name) {
                    processed_libraries.push(name);
                    first_update = true;
                }

                trace!("Calculating total and library progress");
                let processed_count = processed_libraries.len();
                let total_progress =
                    cut_decimal_places(processed_count as f64 / progress.total_files as f64, 2);
                let library_progress = cut_decimal_places(
                    progress.downloaded_bytes as f64 / progress.total_bytes as f64,
                    2,
                );
                tracing::Span::current().record("total_progress", total_progress);
                tracing::Span::current().record("library_progress", library_progress);

                trace!("Download progress");

                if first_update {
                    self.update_progress_ui(
                        &gettext!(
                            "Downloading library ({}/{})",
                            processed_count,
                            progress.total_files,
                        ),
                        total_progress,
                    );
                }
            }
            LibraryInstallationUpdate::Extracting => {
                trace!("Update is an extraction update");

                self.update_progress_ui(&gettext("Extracting libraries..."), 1f64);
            }
        }
    }

    #[instrument(
        name = "process_asset_install_update",
        skip_all,
        fields(
            asset_name = name,
            total_progress,
            asset_progress
        )
    )]
    fn process_asset_install_update(
        &self,
        name: String,
        progress: AssetInstallationUpdate,
        processed_assets: &mut Vec<String>,
    ) {
        match progress {
            AssetInstallationUpdate::Downloading(progress) => {
                trace!("Update is a download update");

                if processed_assets.len() > progress.total_files {
                    debug!("Clearing processed items. Probably new assets are downloaded.");
                    processed_assets.clear();
                }

                let mut first_update = false;
                if !processed_assets.contains(&name) {
                    processed_assets.push(name);
                    first_update = true;
                }

                trace!("Calculating total and asset progress");
                let processed_count = processed_assets.len();
                let total_progress =
                    cut_decimal_places(processed_count as f64 / progress.total_files as f64, 2);
                let asset_progress = cut_decimal_places(
                    progress.downloaded_bytes as f64 / progress.total_bytes as f64,
                    2,
                );
                tracing::Span::current().record("total_progress", total_progress);
                tracing::Span::current().record("asset_progress", asset_progress);

                trace!("Download progress");

                if first_update {
                    self.update_progress_ui(
                        &gettext!(
                            "Downloading asset ({}/{})",
                            processed_count,
                            progress.total_files
                        ),
                        total_progress,
                    );
                }
            }
            AssetInstallationUpdate::Symlink => {
                trace!("Update is a symlinking update");

                self.update_progress_ui(&gettext("Linking assets..."), 1f64);
            }
        }
    }

    #[instrument(name = "process_log_config_install_update", skip_all, fields(progress))]
    fn process_log_config_install_update(&self, progress: LogConfigInstallationUpdate) {
        match progress {
            LogConfigInstallationUpdate::Downloading(progress) => {
                trace!("Received download progress update");

                trace!("Calculating progress");
                let log_config_progress = cut_decimal_places(
                    progress.downloaded_bytes as f64 / progress.total_bytes as f64,
                    2,
                );
                tracing::Span::current().record("progress", log_config_progress);

                trace!("Download progress");

                self.update_progress_ui(
                    &gettext!(
                        "Downloading log config ({}/{})",
                        progress.current_file,
                        progress.total_files
                    ),
                    log_config_progress,
                );
            }
        }
    }

    #[instrument(name = "process_client_install_update", skip_all, fields(progress))]
    fn process_client_install_update(&self, progress: ClientInstallationUpdate) {
        match progress {
            ClientInstallationUpdate::Downloading(progress) => {
                trace!("Received download progress update");

                trace!("Calculating progress");
                let client_progress = cut_decimal_places(
                    progress.downloaded_bytes as f64 / progress.total_bytes as f64,
                    2,
                );
                tracing::Span::current().record("progress", client_progress);

                trace!("Download progress");

                self.update_progress_ui(
                    &gettext!(
                        "Downloading client ({}/{})",
                        progress.current_file,
                        progress.total_files
                    ),
                    client_progress,
                );
            }
        }
    }

    #[instrument(
        name = "update_progress_ui",
        skip_all,
        fields(
            label = label,
            fraction = fraction
        )
    )]
    fn update_progress_ui(&self, label: &str, fraction: f64) {
        let imp = self.imp();

        trace!("Updating the progress UI");
        imp.progress_label.set_label(label);
        imp.progress_bar.set_fraction(fraction);
    }


    #[instrument(name = "cancel_install", skip_all, fields(instance = %self.instance().uuid()))]
    fn cancel_install(&self) {
        debug!("Cancelling installation");

        let handle = self.imp().install_abort_handle.borrow();
        if let Some(handle) = handle.as_ref() {
            trace!("Aborting download future");
            handle.abort();
        }
    }

    #[instrument(name = "edit", skip_all, fields(instance = %self.instance().uuid()))]
    fn edit(&self) {
        debug!("Editing instance");

        trace!("Creating window for editing");
        let instance = self.instance();
        let window = InstancePropertiesWindow::new(instance);

        trace!("Showing window");
        window.show();
    }

    #[instrument(name = "ask_remove", skip_all, fields(instance = %self.instance().uuid()))]
    fn ask_remove(&self) {
        debug!("Asking user for remove confirmation");

        trace!("Creating confirmation dialog");
        let dialog = adw::MessageDialog::new(
            Some(&Application::default().main_window()),
            Some(&gettext("Remove Instance")),
            Some(&gettext("Are you sure you want to remove this instance?")),
        );

        dialog.add_response("remove", &gettext("Remove"));
        dialog.add_response("remove-data", &gettext("Remove with Data"));
        dialog.add_response("cancel", &gettext("Cancel"));

        trace!("Showing dialog");
        dialog.show();

        dialog.connect_response(
            None,
            clone!(@weak self as this => move |dialog, response| {
                trace!("Got a response from the dialog: `{response}`");
                match response {
                    "remove" => this.remove(false),
                    "remove-data" => this.remove(true),
                    _ => {}
                }

                trace!("Closing dialog");
                dialog.close();
            }),
        );
    }

    #[instrument(
        name = "remove",
        skip_all,
        fields(
            instance = %self.instance().uuid(),
            with_data = data,
        )
    )]
    fn remove(&self, data: bool) {
        debug!("Removing instance");
        
        let instance_manager = GInstanceManager::default();
        instance_manager.remove_instance(self.instance().uuid(), data);
        Application::default().toast_notification(&gettext("Removed Instance"));
    }

    #[instrument(name = "set_button_view", skip_all, fields(view=view.get_name()))]
    fn set_button_view(&self, view: ButtonView) {
        match view {
            ButtonView::Launch => {
                self.action_set_enabled("instance.launch", true);
                self.action_set_enabled("instance.stop", false);
                self.action_set_enabled("instance.install", true);
                self.action_set_enabled("instance.cancel-install", false);
            }
            ButtonView::Stop => {
                self.action_set_enabled("instance.launch", false);
                self.action_set_enabled("instance.stop", true);
                self.action_set_enabled("instance.install", false);
                self.action_set_enabled("instance.cancel-install", false);
            }
            ButtonView::Install => {
                self.action_set_enabled("instance.launch", false);
                self.action_set_enabled("instance.stop", false);
                self.action_set_enabled("instance.install", true);
                self.action_set_enabled("instance.cancel-install", false);
            }
            ButtonView::CancelInstall => {
                self.action_set_enabled("instance.launch", false);
                self.action_set_enabled("instance.stop", false);
                self.action_set_enabled("instance.install", false);
                self.action_set_enabled("instance.cancel-install", true);
            }
        }

        trace!("Setting button stack visible child name");
        let imp = self.imp();
        imp.button_stack.set_visible_child_name(view.get_name());
    }

    fn instance(&self) -> GInstance {
        self.property(Self::INSTANCE)
    }

    fn bind_property<T: IsA<gtk::Widget>>(
        &self,
        source_property: &str,
        target: &T,
        target_property: &str,
    ) {
        self.instance()
            .bind_property(source_property, target, target_property)
            .flags(glib::BindingFlags::SYNC_CREATE)
            .build();
    }
}

#[derive(Clone, Debug)]
enum ButtonView {
    Launch,
    Stop,
    Install,
    CancelInstall,
}

impl ButtonView {
    fn get_name(&self) -> &'static str {
        match self {
            ButtonView::Launch => "launch",
            ButtonView::Stop => "stop",
            ButtonView::Install => "install",
            ButtonView::CancelInstall => "cancel-install",
        }
    }
}

fn build_launch_options(instance: &Instance, profile: Option<CobbleProfile>) -> LaunchOptions {
    let mut options = LaunchOptionsBuilder::default();
    options
        .launcher_name(gettext("Cobble"))
        .launcher_version(config::VERSION.to_string())
        .fullscreen(instance.fullscreen || SettingsKey::EnableGlobalFullscreen.get_bool())
        .enable_custom_window_size(
            instance.enable_custom_window_size || SettingsKey::EnableGlobalWindowSize.get_bool(),
        )
        .enable_custom_memory(
            instance.enable_custom_memory || SettingsKey::EnableGlobalMemory.get_bool(),
        );

    // Window Size
    if instance.enable_custom_window_size {
        options
            .custom_width(instance.custom_width)
            .custom_height(instance.custom_height);
    } else if SettingsKey::EnableGlobalWindowSize.get_bool() {
        options
            .custom_width(SettingsKey::GlobalWindowWidth.get_integer() as u32)
            .custom_height(SettingsKey::GlobalWindowHeight.get_integer() as u32);
    }

    // Memory
    if instance.enable_custom_memory {
        options
            .custom_min_memory(instance.custom_min_memory)
            .custom_max_memory(instance.custom_max_memory);
    } else if SettingsKey::EnableGlobalMemory.get_bool() {
        options
            .custom_min_memory(SettingsKey::GlobalMinMemory.get_integer() as u32)
            .custom_max_memory(SettingsKey::GlobalMaxMemory.get_integer() as u32);
    }

    // Profile
    if let Some(profile) = profile {
        options
            .player_name(profile.player_name)
            .profile_id(profile.profile_id)
            .token(profile.minecraft_token);
    }

    if let Some(java) = &instance.custom_java_executable {
        options.java_executable(java);
    } else if !SettingsKey::GlobalJavaExec.get_string().is_empty() {
        options.java_executable(SettingsKey::GlobalJavaExec.get_string());
    }

    if let Some(jvm_arguments) = &instance.custom_jvm_arguments {
        options.custom_jvm_arguments(jvm_arguments);
    } else if SettingsKey::EnableGlobalJvmArgs.get_bool() {
        options.custom_jvm_arguments(SettingsKey::GlobalJvmArgs.get_string());
    }

    // TODO: Env vars

    options.build()
}

fn cut_decimal_places(num: f64, decimals: u32) -> f64 {
    let factor = 10u32.pow(decimals) as f64;
    f64::trunc(num * factor) / factor
}
