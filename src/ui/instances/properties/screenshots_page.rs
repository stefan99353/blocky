use super::{InstancePropertiesWindow, ScreenshotRow};
use crate::gobject::{GInstance, GScreenshot};
use crate::utils::{error_dialog, open_path, spawn_tokio_future, ListStoreExt};
use adw::prelude::*;
use adw::subclass::prelude::*;
use cobble_core::minecraft::Screenshot;
use cobble_core::Instance;
use gio::ListStore;
use glib::{clone, ParamFlags, ParamSpec, ParamSpecObject, WeakRef};
use once_cell::sync::{Lazy, OnceCell};
use std::cell::RefCell;

mod imp {
    use super::*;
    use gtk::CompositeTemplate;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(
        resource = "/com/gitlab/stefan99353/cobble/ui/instances/properties/screenshots_page.ui"
    )]
    pub struct ScreenshotsInstancePropertiesPage {
        #[template_child]
        pub open_screenshot_folder_button: TemplateChild<gtk::Button>,

        #[template_child]
        pub screenshots_stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub screenshots_listbox: TemplateChild<gtk::ListBox>,

        pub instance: RefCell<GInstance>,
        pub window: OnceCell<WeakRef<InstancePropertiesWindow>>,

        pub screenshots: RefCell<ListStore>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ScreenshotsInstancePropertiesPage {
        const NAME: &'static str = "ScreenshotsInstancePropertiesPage";
        type Type = super::ScreenshotsInstancePropertiesPage;
        type ParentType = adw::PreferencesPage;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_instance_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ScreenshotsInstancePropertiesPage {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecObject::new(
                        super::ScreenshotsInstancePropertiesPage::INSTANCE,
                        "Instance",
                        "Instance",
                        GInstance::static_type(),
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecObject::new(
                        super::ScreenshotsInstancePropertiesPage::SCREENSHOTS,
                        "Screenshots",
                        "Screenshots",
                        ListStore::static_type(),
                        ParamFlags::READWRITE,
                    ),
                ]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &glib::Value, pspec: &ParamSpec) {
            match pspec.name() {
                super::ScreenshotsInstancePropertiesPage::INSTANCE => {
                    *self.instance.borrow_mut() = value.get().unwrap()
                }
                super::ScreenshotsInstancePropertiesPage::SCREENSHOTS => {
                    *self.screenshots.borrow_mut() = value.get().unwrap()
                }
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
            match pspec.name() {
                super::ScreenshotsInstancePropertiesPage::INSTANCE => {
                    self.instance.borrow().to_value()
                }
                super::ScreenshotsInstancePropertiesPage::SCREENSHOTS => {
                    self.screenshots.borrow().to_value()
                }
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.instance();

            obj.setup_widgets();
        }
    }

    impl WidgetImpl for ScreenshotsInstancePropertiesPage {}
    impl PreferencesPageImpl for ScreenshotsInstancePropertiesPage {}
}

glib::wrapper! {
    pub struct ScreenshotsInstancePropertiesPage(ObjectSubclass<imp::ScreenshotsInstancePropertiesPage>)
    @extends gtk::Widget, adw::PreferencesPage;
}

#[gtk::template_callbacks]
impl ScreenshotsInstancePropertiesPage {
    pub const INSTANCE: &str = "instance";
    pub const SCREENSHOTS: &str = "screenshots";

    fn setup_widgets(&self) {
        self.set_view(View::Loading);

        // Bind ListBox
        self.imp().screenshots_listbox.bind_model(
            Some(&self.screenshots()),
            clone!(@weak self as this => @default-panic, move |object| {
                let screenshot = object.downcast_ref::<GScreenshot>().unwrap();
                ScreenshotRow::new(screenshot, &this).upcast()
            }),
        );
    }

    #[template_callback]
    fn on_open_folder(&self) {
        // TODO: Use path from instance
        let instance = Instance::from(self.instance());
        let mut path = instance.dot_minecraft_path();
        path.push("screenshots");
        open_path(path);
    }

    pub fn load_screenshots(&self) {
        let ctx = glib::MainContext::default();
        ctx.spawn_local(clone!(@weak self as this => async move {
            let instance = Instance::from(this.instance());

            let screenshots_result = spawn_tokio_future(async move {instance.load_screenshots().await}).await;
            match screenshots_result {
                Ok(screenshots) => {
                    this.set_screenshots(screenshots);
                },
                Err(err) => {
                    error_dialog(err);
                },
            }
        }));
    }

    fn set_screenshots(&self, mut screenshots: Vec<Screenshot>) {
        // Sort screenshots by name
        screenshots.sort_unstable_by(|a, b| b.name.cmp(&a.name));

        // Set screenshots
        let screenshots = screenshots
            .into_iter()
            .map(GScreenshot::from)
            .collect::<Vec<_>>();
        self.screenshots()
            .splice(0, self.screenshots().n_items(), &screenshots);

        self.update_view()
    }

    pub async fn remove_screenshot(&self, screenshot: GScreenshot) {
        let index = self
            .screenshots()
            .index_of::<_, GScreenshot>(|x| screenshot.path() == x.path());
        match index {
            Some(i) => {
                let screenshot = Screenshot::from(screenshot);
                let result = spawn_tokio_future(screenshot.remove()).await;

                match result {
                    Ok(_) => self.screenshots().remove(i),
                    Err(err) => error_dialog(err),
                }
            }
            None => warn!("Trying to remove unknown screenshot"),
        }

        self.update_view();
    }

    fn update_view(&self) {
        match self.screenshots().n_items() {
            0 => self.set_view(View::Empty),
            _ => self.set_view(View::Screenshots),
        }
    }

    fn set_view(&self, view: View) {
        let imp = self.imp();
        imp.screenshots_stack
            .set_visible_child_name(view.get_name());
    }

    fn instance(&self) -> GInstance {
        self.property(Self::INSTANCE)
    }

    fn screenshots(&self) -> ListStore {
        self.property(Self::SCREENSHOTS)
    }

    pub fn set_window(&self, window: WeakRef<InstancePropertiesWindow>) {
        let imp = self.imp();
        imp.window.set(window).unwrap();
    }

    pub fn window(&self) -> WeakRef<InstancePropertiesWindow> {
        let imp = self.imp();
        imp.window.get().unwrap().to_owned()
    }
}

#[derive(Clone, Debug)]
enum View {
    Screenshots,
    Loading,
    Empty,
}

impl View {
    fn get_name(&self) -> &'static str {
        match self {
            View::Screenshots => "screenshots",
            View::Loading => "loading",
            View::Empty => "empty",
        }
    }
}
