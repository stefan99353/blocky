use super::InstancePropertiesWindow;
use crate::gobject::GInstance;
use crate::utils::native_file_chooser;
use adw::prelude::*;
use adw::subclass::prelude::*;
use gettextrs::gettext;
use glib::{clone, BindingFlags, ParamFlags, ParamSpec, ParamSpecObject, WeakRef};
use gtk::FileChooserAction;
use once_cell::sync::{Lazy, OnceCell};
use std::cell::RefCell;

mod imp {
    use super::*;
    use gtk::CompositeTemplate;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/stefan99353/cobble/ui/instances/properties/java_page.ui")]
    pub struct JavaInstancePropertiesPage {
        #[template_child]
        pub override_memory_expander: TemplateChild<adw::ExpanderRow>,
        #[template_child]
        pub min_memory_spinbutton: TemplateChild<gtk::SpinButton>,
        #[template_child]
        pub max_memory_spinbutton: TemplateChild<gtk::SpinButton>,
        #[template_child]
        pub java_exec_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub java_exec_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub jvm_args_entryrow: TemplateChild<adw::EntryRow>,

        pub instance: RefCell<GInstance>,
        pub window: OnceCell<WeakRef<InstancePropertiesWindow>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for JavaInstancePropertiesPage {
        const NAME: &'static str = "JavaInstancePropertiesPage";
        type Type = super::JavaInstancePropertiesPage;
        type ParentType = adw::PreferencesPage;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_instance_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for JavaInstancePropertiesPage {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![ParamSpecObject::new(
                    super::JavaInstancePropertiesPage::INSTANCE,
                    "Instance",
                    "Instance",
                    GInstance::static_type(),
                    ParamFlags::READWRITE,
                )]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &glib::Value, pspec: &ParamSpec) {
            match pspec.name() {
                super::JavaInstancePropertiesPage::INSTANCE => {
                    *self.instance.borrow_mut() = value.get().unwrap()
                }
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
            match pspec.name() {
                super::JavaInstancePropertiesPage::INSTANCE => self.instance.borrow().to_value(),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.instance();

            obj.setup_signals();
        }
    }

    impl WidgetImpl for JavaInstancePropertiesPage {}
    impl PreferencesPageImpl for JavaInstancePropertiesPage {}
}

glib::wrapper! {
    pub struct JavaInstancePropertiesPage(ObjectSubclass<imp::JavaInstancePropertiesPage>)
    @extends gtk::Widget, adw::PreferencesPage;
}

#[gtk::template_callbacks]
impl JavaInstancePropertiesPage {
    pub const INSTANCE: &str = "instance";

    fn setup_signals(&self) {
        self.connect_notify_local(
            Some(Self::INSTANCE),
            clone!(@weak self as this => move |_, _| {
                this.set_bindings();
            }),
        );
    }

    #[template_callback]
    fn on_select_java_exec(&self) {
        let parent = self.imp().window.get().and_then(|w| w.upgrade());

        native_file_chooser(
            Some(&gettext("Select Java Executable")),
            parent.as_ref(),
            FileChooserAction::Open,
            Some(&gettext("Select")),
            Some(&gettext("Cancel")),
            &[],
            true,
        )
        .attach(
            None,
            clone!(@weak self as this => @default-return glib::Continue(false), move |path| {
                if let Some(path) = path {
                    if let Some(path) = path.to_str() {
                        if !path.is_empty() {
                            this.instance().set_custom_java_executable(Some(path.to_string()));
                        }
                    }
                }

                glib::Continue(false)
            }),
        );
    }

    fn set_bindings(&self) {
        let imp = self.imp();

        self.bind_property(
            GInstance::ENABLE_CUSTOM_MEMORY,
            &imp.override_memory_expander.get(),
            "enable-expansion",
            BindingFlags::SYNC_CREATE | BindingFlags::BIDIRECTIONAL,
        );
        self.bind_property(
            GInstance::CUSTOM_MIN_MEMORY,
            &imp.min_memory_spinbutton.get(),
            "value",
            BindingFlags::SYNC_CREATE | BindingFlags::BIDIRECTIONAL,
        );
        self.bind_property(
            GInstance::CUSTOM_MAX_MEMORY,
            &imp.max_memory_spinbutton.get(),
            "value",
            BindingFlags::SYNC_CREATE | BindingFlags::BIDIRECTIONAL,
        );
        self.bind_property(
            GInstance::CUSTOM_JAVA_EXECUTABLE,
            &imp.java_exec_label.get(),
            "label",
            BindingFlags::SYNC_CREATE | BindingFlags::BIDIRECTIONAL,
        );
        self.bind_property(
            GInstance::CUSTOM_JVM_ARGUMENTS,
            &imp.jvm_args_entryrow.get(),
            "text",
            BindingFlags::SYNC_CREATE | BindingFlags::BIDIRECTIONAL,
        );
    }

    fn instance(&self) -> GInstance {
        self.property(Self::INSTANCE)
    }

    fn bind_property<T: IsA<gtk::Widget>>(
        &self,
        source_property: &str,
        target: &T,
        target_property: &str,
        flags: BindingFlags,
    ) {
        self.instance()
            .bind_property(source_property, target, target_property)
            .flags(flags)
            .build();
    }

    pub fn set_window(&self, window: WeakRef<InstancePropertiesWindow>) {
        let imp = self.imp();

        imp.window.set(window).unwrap();
    }
}
