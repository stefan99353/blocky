use super::ScreenshotsInstancePropertiesPage;
use crate::gobject::GScreenshot;
use crate::utils::error_dialog;
use adw::prelude::*;
use adw::subclass::prelude::*;
use gdk::gdk_pixbuf::Pixbuf;
use gettextrs::gettext;
use glib::{clone, ParamFlags, ParamSpec, ParamSpecObject, Priority};
use once_cell::sync::{Lazy, OnceCell};

mod imp {
    use super::*;
    use gtk::CompositeTemplate;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(
        resource = "/com/gitlab/stefan99353/cobble/ui/instances/properties/screenshot_row.ui"
    )]
    pub struct ScreenshotRow {
        #[template_child]
        pub screenshot_image: TemplateChild<gtk::Image>,
        #[template_child]
        pub name_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub path_label: TemplateChild<gtk::Label>,

        pub screenshot: OnceCell<GScreenshot>,
        pub page: OnceCell<ScreenshotsInstancePropertiesPage>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ScreenshotRow {
        const NAME: &'static str = "ScreenshotRow";
        type Type = super::ScreenshotRow;
        type ParentType = gtk::ListBoxRow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_instance_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ScreenshotRow {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecObject::new(
                        super::ScreenshotRow::SCREENSHOT,
                        "Screenshot",
                        "Screenshot",
                        GScreenshot::static_type(),
                        ParamFlags::READWRITE | ParamFlags::CONSTRUCT_ONLY,
                    ),
                    ParamSpecObject::new(
                        super::ScreenshotRow::PAGE,
                        "Page",
                        "Page",
                        ScreenshotsInstancePropertiesPage::static_type(),
                        ParamFlags::READWRITE | ParamFlags::CONSTRUCT_ONLY,
                    ),
                ]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &glib::Value, pspec: &ParamSpec) {
            match pspec.name() {
                super::ScreenshotRow::SCREENSHOT => {
                    self.screenshot.set(value.get().unwrap()).unwrap()
                }
                super::ScreenshotRow::PAGE => self.page.set(value.get().unwrap()).unwrap(),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
            match pspec.name() {
                super::ScreenshotRow::SCREENSHOT => self.screenshot.get().to_value(),
                super::ScreenshotRow::PAGE => self.page.get().to_value(),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.instance();

            obj.setup_widgets();
        }
    }

    impl WidgetImpl for ScreenshotRow {}
    impl ListBoxRowImpl for ScreenshotRow {}
}

glib::wrapper! {
    pub struct ScreenshotRow(ObjectSubclass<imp::ScreenshotRow>)
    @extends gtk::Widget, gtk::ListBoxRow;
}

#[gtk::template_callbacks]
impl ScreenshotRow {
    pub const SCREENSHOT: &str = "screenshot";
    pub const PAGE: &str = "page";

    pub fn new(screenshot: &GScreenshot, page: &ScreenshotsInstancePropertiesPage) -> Self {
        glib::Object::new(&[(Self::SCREENSHOT, screenshot), (Self::PAGE, page)])
    }

    fn setup_widgets(&self) {
        let imp = self.imp();

        self.bind_property("name", &imp.name_label.get(), "label");
        self.bind_property("path", &imp.path_label.get(), "label");

        // Image
        let ctx = glib::MainContext::default();
        ctx.spawn_local(clone!(@weak self as this => async move {
            if let Err(err) = this.load_image().await {
                error_dialog(err);
            }
        }));
    }

    async fn load_image(&self) -> anyhow::Result<()> {
        let path = self.screenshot().path();
        let file = gio::File::for_path(&path);
        let stream = file.read_future(Priority::default()).await?;

        let pixbuf = Pixbuf::from_stream_at_scale_future(&stream, 48, 48, true).await?;
        self.imp().screenshot_image.set_from_pixbuf(Some(&pixbuf));

        Ok(())
    }

    #[template_callback]
    fn on_ask_remove(&self) {
        let parent = self.page().window().upgrade();
        let dialog = adw::MessageDialog::new(
            parent.as_ref(),
            Some(&gettext("Remove Screenshot")),
            Some(&gettext("This removes the file from the disk!")),
        );

        dialog.add_response("remove", &gettext("Remove"));
        dialog.add_response("cancel", &gettext("Cancel"));

        dialog.show();

        dialog.connect_response(
            None,
            clone!(@weak self as this => move |dialog, response| {
                if response == "remove" {
                    this.remove();
                }

                dialog.close();
            }),
        );
    }

    fn remove(&self) {
        let ctx = glib::MainContext::default();
        ctx.spawn_local(clone!(@weak self as this => async move {
            this.page().remove_screenshot(this.screenshot()).await;
        }));
    }

    fn screenshot(&self) -> GScreenshot {
        self.property(Self::SCREENSHOT)
    }

    fn page(&self) -> ScreenshotsInstancePropertiesPage {
        self.property(Self::PAGE)
    }

    fn bind_property<T: IsA<gtk::Widget>>(
        &self,
        source_property: &str,
        target: &T,
        target_property: &str,
    ) {
        self.screenshot()
            .bind_property(source_property, target, target_property)
            .flags(glib::BindingFlags::SYNC_CREATE)
            .build();
    }
}
