use crate::gobject::GInstance;
use crate::utils::{conditional_style, open_path};
use adw::prelude::*;
use adw::subclass::prelude::*;
use cobble_core::Instance;
use glib::{clone, BindingFlags, ParamFlags, ParamSpec, ParamSpecBoolean, ParamSpecObject};
use once_cell::sync::Lazy;
use std::cell::Cell;
use std::cell::RefCell;

mod imp {
    use super::*;
    use gtk::CompositeTemplate;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/stefan99353/cobble/ui/instances/properties/general_page.ui")]
    pub struct GeneralInstancePropertiesPage {
        #[template_child]
        pub name_entryrow: TemplateChild<adw::EntryRow>,
        #[template_child]
        pub description_entryrow: TemplateChild<adw::EntryRow>,
        #[template_child]
        pub version_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub created_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub uuid_label: TemplateChild<gtk::Label>,

        #[template_child]
        pub open_instance_folder_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub open_minecraft_folder_button: TemplateChild<gtk::Button>,

        #[template_child]
        pub instance_path_entryrow: TemplateChild<adw::EntryRow>,
        #[template_child]
        pub libraries_path_entryrow: TemplateChild<adw::EntryRow>,
        #[template_child]
        pub assets_path_entryrow: TemplateChild<adw::EntryRow>,

        pub instance: RefCell<GInstance>,
        pub valid: Cell<bool>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for GeneralInstancePropertiesPage {
        const NAME: &'static str = "GeneralInstancePropertiesPage";
        type Type = super::GeneralInstancePropertiesPage;
        type ParentType = adw::PreferencesPage;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_instance_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for GeneralInstancePropertiesPage {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecObject::new(
                        super::GeneralInstancePropertiesPage::INSTANCE,
                        "Instance",
                        "Instance",
                        GInstance::static_type(),
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecBoolean::new(
                        super::GeneralInstancePropertiesPage::VALID,
                        "Valid",
                        "Valid",
                        true,
                        ParamFlags::READWRITE,
                    ),
                ]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &glib::Value, pspec: &ParamSpec) {
            match pspec.name() {
                super::GeneralInstancePropertiesPage::INSTANCE => {
                    *self.instance.borrow_mut() = value.get().unwrap()
                }
                super::GeneralInstancePropertiesPage::VALID => self.valid.set(value.get().unwrap()),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
            match pspec.name() {
                super::GeneralInstancePropertiesPage::INSTANCE => self.instance.borrow().to_value(),
                super::GeneralInstancePropertiesPage::VALID => self.valid.get().to_value(),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.instance();

            obj.setup_signals();
        }
    }

    impl WidgetImpl for GeneralInstancePropertiesPage {}
    impl PreferencesPageImpl for GeneralInstancePropertiesPage {}
}

glib::wrapper! {
    pub struct GeneralInstancePropertiesPage(ObjectSubclass<imp::GeneralInstancePropertiesPage>)
    @extends gtk::Widget, adw::PreferencesPage;
}

#[gtk::template_callbacks]
impl GeneralInstancePropertiesPage {
    pub const INSTANCE: &str = "instance";
    pub const VALID: &str = "valid";

    fn setup_signals(&self) {
        self.connect_notify_local(
            Some(Self::INSTANCE),
            clone!(@weak self as this => move |_, _| {
                this.set_bindings();
            }),
        );
    }

    #[template_callback]
    fn on_validate(&self) {
        let imp = self.imp();

        let name = imp.name_entryrow.text();
        let valid = !name.is_empty();

        conditional_style(!valid, &imp.name_entryrow.get(), "error");
        self.set_valid(valid);
    }

    #[template_callback]
    fn on_open_instance_folder(&self) {
        let instance = Instance::from(self.instance());
        open_path(instance.instance_path());
    }

    #[template_callback]
    fn on_open_minecraft_folder(&self) {
        let instance = Instance::from(self.instance());
        open_path(instance.dot_minecraft_path());
    }

    fn set_bindings(&self) {
        let imp = self.imp();

        self.bind_property(
            GInstance::NAME,
            &imp.name_entryrow.get(),
            "text",
            BindingFlags::SYNC_CREATE | BindingFlags::BIDIRECTIONAL,
        );
        self.bind_property(
            GInstance::DESCRIPTION,
            &imp.description_entryrow.get(),
            "text",
            BindingFlags::SYNC_CREATE | BindingFlags::BIDIRECTIONAL,
        );
        self.bind_property(
            GInstance::VERSION,
            &imp.version_label.get(),
            "label",
            BindingFlags::SYNC_CREATE,
        );
        self.bind_property(
            GInstance::UUID,
            &imp.uuid_label.get(),
            "label",
            BindingFlags::SYNC_CREATE,
        );
        self.bind_property(
            GInstance::INSTANCE_PATH,
            &imp.instance_path_entryrow.get(),
            "text",
            BindingFlags::SYNC_CREATE,
        );
        self.bind_property(
            GInstance::LIBRARIES_PATH,
            &imp.libraries_path_entryrow.get(),
            "text",
            BindingFlags::SYNC_CREATE,
        );
        self.bind_property(
            GInstance::ASSETS_PATH,
            &imp.assets_path_entryrow.get(),
            "text",
            BindingFlags::SYNC_CREATE,
        );

        // Created
        let created = self
            .instance()
            .created()
            .format(&time::format_description::well_known::Rfc2822)
            .unwrap();
        imp.created_label.set_label(&created);
    }

    fn instance(&self) -> GInstance {
        self.property(Self::INSTANCE)
    }

    pub fn valid(&self) -> bool {
        self.property(Self::VALID)
    }

    fn set_valid(&self, value: bool) {
        self.set_property(Self::VALID, value);
    }

    fn bind_property<T: IsA<gtk::Widget>>(
        &self,
        source_property: &str,
        target: &T,
        target_property: &str,
        flags: BindingFlags,
    ) {
        self.instance()
            .bind_property(source_property, target, target_property)
            .flags(flags)
            .build();
    }
}
