use super::{InstancePropertiesWindow, ServerRow};
use crate::gobject::{GInstance, GServer};
use crate::utils::{error_dialog, spawn_tokio_future, ListStoreExt};
use adw::prelude::*;
use adw::subclass::prelude::*;
use cobble_core::minecraft::Server;
use cobble_core::Instance;
use gio::ListStore;
use glib::{clone, ParamFlags, ParamSpec, ParamSpecObject, WeakRef};
use once_cell::sync::{Lazy, OnceCell};
use std::cell::RefCell;

mod imp {
    use super::*;
    use gtk::CompositeTemplate;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/stefan99353/cobble/ui/instances/properties/servers_page.ui")]
    pub struct ServersInstancePropertiesPage {
        #[template_child]
        pub servers_stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub servers_listbox: TemplateChild<gtk::ListBox>,

        pub instance: RefCell<GInstance>,
        pub window: OnceCell<WeakRef<InstancePropertiesWindow>>,

        pub servers: RefCell<ListStore>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ServersInstancePropertiesPage {
        const NAME: &'static str = "ServersInstancePropertiesPage";
        type Type = super::ServersInstancePropertiesPage;
        type ParentType = adw::PreferencesPage;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ServersInstancePropertiesPage {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecObject::new(
                        super::ServersInstancePropertiesPage::INSTANCE,
                        "Instance",
                        "Instance",
                        GInstance::static_type(),
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecObject::new(
                        super::ServersInstancePropertiesPage::SERVERS,
                        "Servers",
                        "Servers",
                        ListStore::static_type(),
                        ParamFlags::READWRITE,
                    ),
                ]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &glib::Value, pspec: &ParamSpec) {
            match pspec.name() {
                super::ServersInstancePropertiesPage::INSTANCE => {
                    *self.instance.borrow_mut() = value.get().unwrap()
                }
                super::ServersInstancePropertiesPage::SERVERS => {
                    *self.servers.borrow_mut() = value.get().unwrap()
                }
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
            match pspec.name() {
                super::ServersInstancePropertiesPage::INSTANCE => self.instance.borrow().to_value(),
                super::ServersInstancePropertiesPage::SERVERS => self.servers.borrow().to_value(),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.instance();

            obj.setup_widgets();
        }
    }

    impl WidgetImpl for ServersInstancePropertiesPage {}
    impl PreferencesPageImpl for ServersInstancePropertiesPage {}
}

glib::wrapper! {
    pub struct ServersInstancePropertiesPage(ObjectSubclass<imp::ServersInstancePropertiesPage>)
    @extends gtk::Widget, adw::PreferencesPage;
}

impl ServersInstancePropertiesPage {
    pub const INSTANCE: &str = "instance";
    pub const SERVERS: &str = "servers";

    fn setup_widgets(&self) {
        self.set_view(View::Loading);

        // Bind ListBox
        self.imp().servers_listbox.bind_model(
            Some(&self.servers()),
            clone!(@weak self as this => @default-panic, move |object| {
                let server = object.downcast_ref::<GServer>().unwrap();
                ServerRow::new(server, &this).upcast()
            }),
        );
    }

    pub fn load_servers(&self) {
        let ctx = glib::MainContext::default();
        ctx.spawn_local(clone!(@weak self as this => async move {
            let instance = Instance::from(this.instance());

            let servers_result = spawn_tokio_future(async move {instance.load_servers().await}).await;
            match servers_result {
                Ok(servers) => {
                    this.set_servers(servers);
                },
                Err(err) => {
                    error_dialog(err);
                },
            }
        }));
    }

    fn set_servers(&self, mut servers: Vec<Server>) {
        // Sort servers by name
        servers.sort_unstable_by(|a, b| a.name.cmp(&b.name));

        // Set servers
        let servers = servers.into_iter().map(GServer::from).collect::<Vec<_>>();
        self.servers().splice(0, self.servers().n_items(), &servers);

        self.update_view();
    }

    pub async fn remove_server(&self, server: GServer) {
        let index = self
            .servers()
            .index_of::<_, GServer>(|x| server.path() == x.path());
        match index {
            Some(i) => {
                let server = Server::from(server);
                let result = spawn_tokio_future(server.remove()).await;

                match result {
                    Ok(_) => self.servers().remove(i),
                    Err(err) => error_dialog(err),
                }
            }
            None => warn!("Trying to remove unknown server"),
        }

        self.update_view();
    }

    fn update_view(&self) {
        match self.servers().n_items() {
            0 => self.set_view(View::Empty),
            _ => self.set_view(View::Servers),
        }
    }

    fn set_view(&self, view: View) {
        let imp = self.imp();
        imp.servers_stack.set_visible_child_name(view.get_name());
    }

    fn instance(&self) -> GInstance {
        self.property(Self::INSTANCE)
    }

    fn servers(&self) -> ListStore {
        self.property(Self::SERVERS)
    }

    pub fn set_window(&self, window: WeakRef<InstancePropertiesWindow>) {
        let imp = self.imp();
        imp.window.set(window).unwrap();
    }

    pub fn window(&self) -> WeakRef<InstancePropertiesWindow> {
        let imp = self.imp();
        imp.window.get().unwrap().to_owned()
    }
}

#[derive(Clone, Debug)]
enum View {
    Servers,
    Loading,
    Empty,
}

impl View {
    fn get_name(&self) -> &'static str {
        match self {
            View::Servers => "servers",
            View::Loading => "loading",
            View::Empty => "empty",
        }
    }
}
