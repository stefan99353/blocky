use crate::gobject::GInstance;
use adw::prelude::*;
use adw::subclass::prelude::*;
use glib::{clone, BindingFlags, ParamFlags, ParamSpec, ParamSpecObject};
use once_cell::sync::Lazy;
use std::cell::RefCell;

mod imp {
    use super::*;
    use gtk::CompositeTemplate;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/stefan99353/cobble/ui/instances/properties/game_page.ui")]
    pub struct GameInstancePropertiesPage {
        #[template_child]
        pub enable_fullscreen_switch: TemplateChild<gtk::Switch>,
        #[template_child]
        pub override_window_size_expander: TemplateChild<adw::ExpanderRow>,
        #[template_child]
        pub window_width_spinbutton: TemplateChild<gtk::SpinButton>,
        #[template_child]
        pub window_height_spinbutton: TemplateChild<gtk::SpinButton>,

        pub instance: RefCell<GInstance>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for GameInstancePropertiesPage {
        const NAME: &'static str = "GameInstancePropertiesPage";
        type Type = super::GameInstancePropertiesPage;
        type ParentType = adw::PreferencesPage;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for GameInstancePropertiesPage {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![ParamSpecObject::new(
                    super::GameInstancePropertiesPage::INSTANCE,
                    "Instance",
                    "Instance",
                    GInstance::static_type(),
                    ParamFlags::READWRITE,
                )]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &glib::Value, pspec: &ParamSpec) {
            match pspec.name() {
                super::GameInstancePropertiesPage::INSTANCE => {
                    *self.instance.borrow_mut() = value.get().unwrap()
                }
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
            match pspec.name() {
                super::GameInstancePropertiesPage::INSTANCE => self.instance.borrow().to_value(),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.instance();

            obj.setup_signals();
        }
    }

    impl WidgetImpl for GameInstancePropertiesPage {}
    impl PreferencesPageImpl for GameInstancePropertiesPage {}
}

glib::wrapper! {
    pub struct GameInstancePropertiesPage(ObjectSubclass<imp::GameInstancePropertiesPage>)
    @extends gtk::Widget, adw::PreferencesPage;
}

impl GameInstancePropertiesPage {
    pub const INSTANCE: &str = "instance";

    fn setup_signals(&self) {
        self.connect_notify_local(
            Some(Self::INSTANCE),
            clone!(@weak self as this => move |_, _| {
                this.set_bindings();
            }),
        );
    }

    fn set_bindings(&self) {
        let imp = self.imp();

        self.bind_property(
            GInstance::FULLSCREEN,
            &imp.enable_fullscreen_switch.get(),
            "state",
            BindingFlags::SYNC_CREATE | BindingFlags::BIDIRECTIONAL,
        );
        self.bind_property(
            GInstance::ENABLE_CUSTOM_WINDOW_SIZE,
            &imp.override_window_size_expander.get(),
            "enable-expansion",
            BindingFlags::SYNC_CREATE | BindingFlags::BIDIRECTIONAL,
        );
        self.bind_property(
            GInstance::CUSTOM_WIDTH,
            &imp.window_width_spinbutton.get(),
            "value",
            BindingFlags::SYNC_CREATE | BindingFlags::BIDIRECTIONAL,
        );
        self.bind_property(
            GInstance::CUSTOM_HEIGHT,
            &imp.window_height_spinbutton.get(),
            "value",
            BindingFlags::SYNC_CREATE | BindingFlags::BIDIRECTIONAL,
        );
    }

    fn instance(&self) -> GInstance {
        self.property(Self::INSTANCE)
    }

    fn bind_property<T: IsA<gtk::Widget>>(
        &self,
        source_property: &str,
        target: &T,
        target_property: &str,
        flags: BindingFlags,
    ) {
        self.instance()
            .bind_property(source_property, target, target_property)
            .flags(flags)
            .build();
    }
}
