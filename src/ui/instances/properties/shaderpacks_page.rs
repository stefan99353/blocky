use super::{InstancePropertiesWindow, ShaderpackRow};
use crate::gobject::{GInstance, GShaderpack};
use crate::utils::{
    error_dialog, native_file_chooser_async, open_path, spawn_tokio_future, ListStoreExt,
};
use adw::prelude::*;
use adw::subclass::prelude::*;
use cobble_core::minecraft::Shaderpack;
use cobble_core::Instance;
use gettextrs::gettext;
use gio::ListStore;
use glib::{clone, ParamFlags, ParamSpec, ParamSpecObject, WeakRef};
use gtk::{FileChooserAction, FileFilter};
use once_cell::sync::{Lazy, OnceCell};
use std::cell::RefCell;

mod imp {
    use super::*;
    use gtk::CompositeTemplate;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(
        resource = "/com/gitlab/stefan99353/cobble/ui/instances/properties/shaderpacks_page.ui"
    )]
    pub struct ShaderpacksInstancePropertiesPage {
        #[template_child]
        pub open_shaderpack_folder_button: TemplateChild<gtk::Button>,

        #[template_child]
        pub shaderpacks_stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub shaderpacks_listbox: TemplateChild<gtk::ListBox>,

        pub instance: RefCell<GInstance>,
        pub window: OnceCell<WeakRef<InstancePropertiesWindow>>,

        pub shaderpacks: RefCell<ListStore>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ShaderpacksInstancePropertiesPage {
        const NAME: &'static str = "ShaderpacksInstancePropertiesPage";
        type Type = super::ShaderpacksInstancePropertiesPage;
        type ParentType = adw::PreferencesPage;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_instance_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ShaderpacksInstancePropertiesPage {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecObject::new(
                        super::ShaderpacksInstancePropertiesPage::INSTANCE,
                        "Instance",
                        "Instance",
                        GInstance::static_type(),
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecObject::new(
                        super::ShaderpacksInstancePropertiesPage::SHADERPACKS,
                        "Shaderpacks",
                        "Shaderpacks",
                        ListStore::static_type(),
                        ParamFlags::READWRITE,
                    ),
                ]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &glib::Value, pspec: &ParamSpec) {
            match pspec.name() {
                super::ShaderpacksInstancePropertiesPage::INSTANCE => {
                    *self.instance.borrow_mut() = value.get().unwrap()
                }
                super::ShaderpacksInstancePropertiesPage::SHADERPACKS => {
                    *self.shaderpacks.borrow_mut() = value.get().unwrap()
                }
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
            match pspec.name() {
                super::ShaderpacksInstancePropertiesPage::INSTANCE => {
                    self.instance.borrow().to_value()
                }
                super::ShaderpacksInstancePropertiesPage::SHADERPACKS => {
                    self.shaderpacks.borrow().to_value()
                }
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.instance();

            obj.setup_widgets();
        }
    }

    impl WidgetImpl for ShaderpacksInstancePropertiesPage {}
    impl PreferencesPageImpl for ShaderpacksInstancePropertiesPage {}
}

glib::wrapper! {
    pub struct ShaderpacksInstancePropertiesPage(ObjectSubclass<imp::ShaderpacksInstancePropertiesPage>)
    @extends gtk::Widget, adw::PreferencesPage;
}

#[gtk::template_callbacks]
impl ShaderpacksInstancePropertiesPage {
    pub const INSTANCE: &str = "instance";
    pub const SHADERPACKS: &str = "shaderpacks";

    fn setup_widgets(&self) {
        self.set_view(View::Loading);

        // Bind ListBox
        self.imp().shaderpacks_listbox.bind_model(
            Some(&self.shaderpacks()),
            clone!(@weak self as this => @default-panic, move |object| {
                let shaderpack = object.downcast_ref::<GShaderpack>().unwrap();
                ShaderpackRow::new(shaderpack, &this).upcast()
            }),
        );
    }

    #[template_callback]
    async fn on_add_shaderpack_clicked(&self) {
        let parent = self.imp().window.get().and_then(|w| w.upgrade());

        let filter = FileFilter::new();
        filter.add_mime_type("application/zip");
        filter.set_name(Some(&gettext("Archive (.zip)")));

        let path = native_file_chooser_async(
            Some(&gettext("Add Shaderpack")),
            parent.as_ref(),
            FileChooserAction::Open,
            Some(&gettext("Add")),
            Some(&gettext("Cancel")),
            &[filter],
            true,
        )
        .await;

        let instance = Instance::from(self.instance());
        if let Some(src) = path {
            let add_future = spawn_tokio_future(async move { instance.add_shaderpack(src).await });

            match add_future.await {
                Ok(Some(shaderpack)) => {
                    self.add_shaderpack(shaderpack);
                }
                Ok(None) => {
                    error_dialog(gettext("The selected file is not a valid shaderpack"));
                }
                Err(err) => {
                    error_dialog(err);
                }
            }
        }
    }

    #[template_callback]
    fn on_open_folder(&self) {
        let instance = Instance::from(self.instance());
        open_path(instance.shaderpacks_path());
    }

    pub fn load_shaderpacks(&self) {
        let ctx = glib::MainContext::default();
        ctx.spawn_local(clone!(@weak self as this => async move {
            let instance = Instance::from(this.instance());

            let shaderpacks_result = spawn_tokio_future(async move {instance.load_shaderpacks().await}).await;
            match shaderpacks_result {
                Ok(shaderpacks) => {
                    this.set_shaderpacks(shaderpacks);
                },
                Err(err) => {
                    error_dialog(err);
                },
            }
        }));
    }

    fn set_shaderpacks(&self, mut shaderpacks: Vec<Shaderpack>) {
        // Sort shaderpacks by name
        shaderpacks.sort_unstable_by(|a, b| a.name.cmp(&b.name));

        // Set shaderpacks
        let shaderpacks = shaderpacks
            .into_iter()
            .map(GShaderpack::from)
            .collect::<Vec<_>>();
        self.shaderpacks()
            .splice(0, self.shaderpacks().n_items(), &shaderpacks);

        self.update_view();
    }

    fn add_shaderpack(&self, shaderpack: Shaderpack) {
        let shaderpack = GShaderpack::from(shaderpack);
        self.shaderpacks().append(&shaderpack);

        self.update_view();
    }

    pub async fn remove_shaderpack(&self, shaderpack: GShaderpack) {
        let index = self
            .shaderpacks()
            .index_of::<_, GShaderpack>(|x| shaderpack.path() == x.path());
        match index {
            Some(i) => {
                let shaderpack = Shaderpack::from(shaderpack);
                let result = spawn_tokio_future(shaderpack.remove()).await;

                match result {
                    Ok(_) => self.shaderpacks().remove(i),
                    Err(err) => error_dialog(err),
                }
            }
            None => warn!("Trying to remove unknown shaderpack"),
        }

        self.update_view();
    }

    fn update_view(&self) {
        match self.shaderpacks().n_items() {
            0 => self.set_view(View::Empty),
            _ => self.set_view(View::Shaderpacks),
        }
    }

    fn set_view(&self, view: View) {
        let imp = self.imp();
        imp.shaderpacks_stack
            .set_visible_child_name(view.get_name());
    }

    fn instance(&self) -> GInstance {
        self.property(Self::INSTANCE)
    }

    fn shaderpacks(&self) -> ListStore {
        self.property(Self::SHADERPACKS)
    }

    pub fn set_window(&self, window: WeakRef<InstancePropertiesWindow>) {
        let imp = self.imp();
        imp.window.set(window).unwrap();
    }

    pub fn window(&self) -> WeakRef<InstancePropertiesWindow> {
        let imp = self.imp();
        imp.window.get().unwrap().to_owned()
    }
}

#[derive(Clone, Debug)]
enum View {
    Shaderpacks,
    Loading,
    Empty,
}

impl View {
    fn get_name(&self) -> &'static str {
        match self {
            View::Shaderpacks => "shaderpacks",
            View::Loading => "loading",
            View::Empty => "empty",
        }
    }
}
