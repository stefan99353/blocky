use super::instance_row::InstanceRow;
use crate::gobject::{GInstance, GInstanceManager};
use adw::prelude::*;
use adw::subclass::prelude::*;

mod imp {
    use super::*;
    use gtk::CompositeTemplate;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/stefan99353/cobble/ui/instances/instance_page.ui")]
    pub struct InstancePage {
        #[template_child]
        pub instances_listbox: TemplateChild<gtk::ListBox>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for InstancePage {
        const NAME: &'static str = "InstancePage";
        type Type = super::InstancePage;
        type ParentType = adw::PreferencesPage;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for InstancePage {
        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.instance();

            obj.setup_widgets();
        }
    }

    impl WidgetImpl for InstancePage {}
    impl PreferencesPageImpl for InstancePage {}
}

glib::wrapper! {
    pub struct InstancePage(ObjectSubclass<imp::InstancePage>)
    @extends gtk::Widget, adw::PreferencesPage;
}

impl InstancePage {
    fn setup_widgets(&self) {
        let imp = self.imp();
        let instance_manager = GInstanceManager::default();

        imp.instances_listbox
            .bind_model(Some(&instance_manager.instances()), |object| {
                let instance = object.downcast_ref::<GInstance>().unwrap();
                InstanceRow::new(instance).upcast()
            });
    }
}
