use crate::gobject::GProfileManager;
use crate::utils::error_dialog;
use crate::{config, utils};
use adw::prelude::*;
use adw::subclass::prelude::*;
use cobble_core::profile::CobbleProfile;
use futures::future::{AbortHandle, Abortable};
use glib::clone;
use std::cell::RefCell;

mod imp {
    use super::*;
    use gtk::CompositeTemplate;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/stefan99353/cobble/ui/profiles/new_profile_window.ui")]
    pub struct NewProfileWindow {
        #[template_child]
        pub cancel_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub copy_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub prep_spinner: TemplateChild<gtk::Spinner>,
        #[template_child]
        pub stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub verification_url_button: TemplateChild<gtk::LinkButton>,
        #[template_child]
        pub user_code_label: TemplateChild<gtk::Label>,

        pub abort_handle: RefCell<Option<AbortHandle>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for NewProfileWindow {
        const NAME: &'static str = "NewProfileWindow";
        type Type = super::NewProfileWindow;
        type ParentType = adw::Window;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_instance_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for NewProfileWindow {
        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.instance();

            if config::PROFILE == "Devel" {
                obj.add_css_class("devel");
            }

            obj.set_view(super::View::Preparing);
            obj.setup_authentication();
        }
    }

    impl WidgetImpl for NewProfileWindow {}
    impl WindowImpl for NewProfileWindow {}
    impl AdwWindowImpl for NewProfileWindow {}
}

glib::wrapper! {
    pub struct NewProfileWindow(ObjectSubclass<imp::NewProfileWindow>)
    @extends gtk::Widget, gtk::Window, adw::Window;
}

#[gtk::template_callbacks]
impl NewProfileWindow {
    pub fn new() -> Self {
        glib::Object::new(&[])
    }

    #[template_callback]
    #[instrument(name = "on_cancel_clicked", skip_all)]
    fn on_cancel_clicked(&self) {
        let handle = self.imp().abort_handle.borrow();
        if let Some(handle) = handle.as_ref() {
            trace!("Aborting authentication future");
            handle.abort();
            trace!("Closing window");
            self.close();
        }
    }

    #[template_callback]
    #[instrument(name = "on_copy_clicked", skip_all, fields(code))]
    fn on_copy_clicked(&self) {
        if let Some(display) = gdk::Display::default() {
            trace!("Getting code");
            let code = self.imp().user_code_label.label();
            tracing::Span::current().record("code", code.to_string());

            trace!("Setting clipboard text");
            let clipboard = display.clipboard();
            clipboard.set_text(code.as_str());
        }
    }

    #[instrument(name = "setup_authentication", skip_all)]
    fn setup_authentication(&self) {
        let ctx = glib::MainContext::default();
        ctx.spawn_local(clone!(@weak self as this => async move {
            let (abort_handle, abort_registration) = AbortHandle::new_pair();
            let auth_future = Abortable::new(this.authenticate(), abort_registration);
            *this.imp().abort_handle.borrow_mut() = Some(abort_handle);

            trace!("Starting authentication process");
            match auth_future.await {
                Ok(_) => trace!("Authentication finished"),
                Err(_) => trace!("Authentication process aborted"),
            }
        }));
    }

    #[instrument(name = "authenticate", skip_all, fields(code, url))]
    async fn authenticate(&self) {
        let imp = self.imp();

        trace!("Setting up cobble-core authentication to get the code and URL for authentication");
        let user_code = match utils::spawn_tokio_future(CobbleProfile::setup_authentication(
            config::MS_GRAPH_ID.to_string(),
        ))
        .await
        {
            Ok(code) => code,
            Err(err) => {
                error_dialog(err);
                return;
            }
        };

        let code = user_code.user_code();
        let url = user_code.verification_url();

        tracing::Span::current().record("code", &code);
        tracing::Span::current().record("url", &url);

        trace!("Updating UI with code and URL");
        imp.user_code_label.set_label(&code);
        imp.verification_url_button.set_label(&url);
        imp.verification_url_button.set_uri(&url);

        imp.copy_button.set_sensitive(true);
        self.set_view(View::Ready);

        trace!("Waiting for authentication to finish");
        let profile = match utils::spawn_tokio_future(CobbleProfile::authenticate(user_code)).await
        {
            Ok(profile) => profile,
            Err(err) => {
                error_dialog(err);
                return;
            }
        };

        debug!("Adding new profile");
        let profile_manager = GProfileManager::default();
        profile_manager.add_profile(profile.into());

        trace!("Closing window");
        self.close();
    }

    #[instrument(name = "set_view", skip_all, fields(view=view.get_name()))]
    fn set_view(&self, view: View) {
        let imp = self.imp();

        match view {
            View::Preparing => {
                imp.prep_spinner.set_spinning(true);
            }
            View::Ready => {}
        }

        trace!("Setting stack visible child name");
        imp.stack.set_visible_child_name(view.get_name());
    }
}

impl Default for NewProfileWindow {
    fn default() -> Self {
        Self::new()
    }
}

#[derive(Clone, Debug)]
enum View {
    Preparing,
    Ready,
}

impl View {
    fn get_name(&self) -> &'static str {
        match self {
            View::Preparing => "preparing",
            View::Ready => "ready",
        }
    }
}
