mod error;
mod file_chooser;
mod instances;
mod list_store;
mod open;
mod profiles;
mod runtime;
mod style;

pub use self::open::*;
pub use error::*;
pub use file_chooser::*;
pub use instances::*;
pub use list_store::*;
pub use profiles::*;
pub use runtime::*;
pub use style::*;

use serde::{Deserialize, Serialize};
use std::path::Path;
use tokio::fs::File;
use tokio::io::{AsyncReadExt, AsyncWriteExt};

pub async fn read_file<T>(path: impl AsRef<Path>) -> anyhow::Result<T>
where
    T: Default + for<'de> Deserialize<'de>,
{
    let mut result = T::default();

    if path.as_ref().is_file() {
        let mut file = File::open(path).await?;
        let mut buf = vec![];
        file.read_to_end(&mut buf).await?;
        result = serde_json::from_slice(&buf)?;
    }

    Ok(result)
}

pub async fn write_file<T>(path: impl AsRef<Path>, value: &T) -> anyhow::Result<()>
where
    T: Serialize,
{
    let data = serde_json::to_vec_pretty(value)?;
    let mut file = File::create(path).await?;
    file.write_all(&data).await?;

    Ok(())
}
