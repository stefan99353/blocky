use gdk::prelude::ListModelExt;
use gio::ListStore;
use glib::Cast;

pub trait ListStoreExt {
    fn index_of<P, T>(&self, predicate: P) -> Option<u32>
    where
        P: FnMut(T) -> bool,
        T: glib::IsA<glib::Object>;
}

impl ListStoreExt for ListStore {
    fn index_of<P, T>(&self, mut predicate: P) -> Option<u32>
    where
        P: FnMut(T) -> bool,
        T: glib::IsA<glib::Object>,
    {
        for pos in 0..self.n_items() {
            let tmp = self.item(pos).unwrap().downcast::<T>().unwrap();

            if predicate(tmp) {
                return Some(pos);
            }
        }

        None
    }
}
