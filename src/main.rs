#![warn(
    missing_debug_implementations,
    clippy::print_stderr,
    clippy::print_stdout
)]

use application::Application;
use tracing_subscriber::{EnvFilter, FmtSubscriber};

#[macro_use]
extern crate tracing;

mod application;
#[rustfmt::skip]
mod config;
mod gobject;
mod paths;
mod settings;
mod ui;
mod utils;
// mod traits;

fn main() {
    // Initialize Logger
    FmtSubscriber::builder()
        .with_env_filter(EnvFilter::from_default_env())
        .init();

    if cfg!(debug_assertions) {
        warn!("<<=== THIS IS A DEBUG BUILD ===>>");
    }

    // Initialize GTK and Libadwaita
    gtk::init().expect("Failed to initialize GTK");
    adw::init().expect("Failed to initialize ADW");

    // Initialize paths
    paths::init().expect("Failed to create necessary directories");

    // Initialize i18n
    gettextrs::setlocale(gettextrs::LocaleCategory::LcAll, "");
    gettextrs::bindtextdomain(config::GETTEXT_PACKAGE, config::LOCALEDIR)
        .expect("Unable to bind the text domain");
    gettextrs::textdomain(config::GETTEXT_PACKAGE).expect("Unable to switch to the text domain");

    // Initialize gresources
    let resource =
        gio::Resource::load(config::RESOURCES_FILE).expect("Could not load gresource file");
    gio::resources_register(&resource);

    // Set application name
    glib::set_application_name(&gettextrs::gettext("Cobble"));

    let application = Application::new();
    application.run();
}
