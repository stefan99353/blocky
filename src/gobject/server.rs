use cobble_core::minecraft::{AcceptTextures, Server};
use gdk::subclass::prelude::*;
use glib::{ObjectExt, ParamFlags, ParamSpec, ParamSpecString, ToValue};
use once_cell::sync::Lazy;
use std::cell::RefCell;
use std::path::PathBuf;

mod imp {
    use super::*;

    #[derive(Debug, Default)]
    pub struct GServer {
        name: RefCell<String>,
        ip: RefCell<String>,
        accept_textures: RefCell<String>,
        path: RefCell<String>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for GServer {
        const NAME: &'static str = "GServer";
        type Type = super::GServer;
        type ParentType = glib::Object;
    }

    impl ObjectImpl for GServer {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecString::new(
                        super::GServer::NAME,
                        "Name",
                        "Name",
                        None,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecString::new(
                        super::GServer::IP,
                        "IP",
                        "IP",
                        None,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecString::new(
                        super::GServer::ACCEPT_TEXTURES,
                        "Accept Textures",
                        "Accept Textures",
                        None,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecString::new(
                        super::GServer::PATH,
                        "Path",
                        "Path",
                        None,
                        ParamFlags::READWRITE,
                    ),
                ]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &glib::Value, pspec: &ParamSpec) {
            match pspec.name() {
                super::GServer::NAME => *self.name.borrow_mut() = value.get().unwrap(),
                super::GServer::IP => *self.ip.borrow_mut() = value.get().unwrap(),
                super::GServer::ACCEPT_TEXTURES => {
                    *self.accept_textures.borrow_mut() = value.get().unwrap()
                }
                super::GServer::PATH => *self.path.borrow_mut() = value.get().unwrap(),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
            match pspec.name() {
                super::GServer::NAME => self.name.borrow().to_value(),
                super::GServer::IP => self.ip.borrow().to_value(),
                super::GServer::ACCEPT_TEXTURES => self.accept_textures.borrow().to_value(),
                super::GServer::PATH => self.path.borrow().to_value(),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }
    }
}

glib::wrapper! {
    pub struct GServer(ObjectSubclass<imp::GServer>);
}

impl GServer {
    pub const NAME: &str = "name";
    pub const IP: &str = "ip";
    pub const ACCEPT_TEXTURES: &str = "accept-textures";
    pub const PATH: &str = "path";

    pub fn set_name(&self, value: String) {
        self.set_property(Self::NAME, value);
    }

    pub fn name(&self) -> String {
        self.property(Self::NAME)
    }

    pub fn set_ip(&self, value: String) {
        self.set_property(Self::IP, value);
    }

    pub fn ip(&self) -> String {
        self.property(Self::IP)
    }

    pub fn set_accept_textures(&self, value: AcceptTextures) {
        let value = match value {
            AcceptTextures::Prompt => "prompt",
            AcceptTextures::Enabled => "enabled",
            AcceptTextures::Disabled => "disabled",
        };
        self.set_property(Self::ACCEPT_TEXTURES, value);
    }

    pub fn accept_textures(&self) -> AcceptTextures {
        let value: String = self.property(Self::ACCEPT_TEXTURES);

        match value.as_str() {
            "prompt" => AcceptTextures::Prompt,
            "enabled" => AcceptTextures::Enabled,
            "disabled" => AcceptTextures::Disabled,
            g => panic!("Invalid accept_textures string '{g}'"),
        }
    }

    pub fn set_path(&self, value: PathBuf) {
        self.set_property(Self::PATH, value.to_string_lossy().to_string());
    }

    pub fn path(&self) -> PathBuf {
        let value: String = self.property(Self::PATH);
        PathBuf::from(value)
    }
}

impl From<Server> for GServer {
    fn from(value: Server) -> Self {
        let object: Self = glib::Object::new(&[
            (Self::NAME, &value.name),
            (Self::IP, &value.ip),
            (Self::PATH, &value.path.to_string_lossy().to_string()),
        ]);

        object.set_accept_textures(value.accept_textures);

        object
    }
}

impl From<GServer> for Server {
    fn from(value: GServer) -> Self {
        Self {
            name: value.name(),
            ip: value.ip(),
            accept_textures: value.accept_textures(),
            path: value.path(),
        }
    }
}
