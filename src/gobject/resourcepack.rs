use cobble_core::minecraft::{Resourcepack, ResourcepackType};
use gdk::subclass::prelude::*;
use glib::{ObjectExt, ParamFlags, ParamSpec, ParamSpecString, ToValue};
use once_cell::sync::Lazy;
use std::cell::RefCell;
use std::path::PathBuf;

mod imp {
    use super::*;

    #[derive(Debug, Default)]
    pub struct GResourcepack {
        name: RefCell<String>,
        path: RefCell<String>,
        type_: RefCell<String>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for GResourcepack {
        const NAME: &'static str = "GResourcepack";
        type Type = super::GResourcepack;
        type ParentType = glib::Object;
    }

    impl ObjectImpl for GResourcepack {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecString::new(
                        super::GResourcepack::NAME,
                        "Name",
                        "Name",
                        None,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecString::new(
                        super::GResourcepack::PATH,
                        "Path",
                        "Path",
                        None,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecString::new(
                        super::GResourcepack::TYPE,
                        "Type",
                        "Type",
                        None,
                        ParamFlags::READWRITE,
                    ),
                ]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &glib::Value, pspec: &ParamSpec) {
            match pspec.name() {
                super::GResourcepack::NAME => *self.name.borrow_mut() = value.get().unwrap(),
                super::GResourcepack::PATH => *self.path.borrow_mut() = value.get().unwrap(),
                super::GResourcepack::TYPE => *self.type_.borrow_mut() = value.get().unwrap(),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
            match pspec.name() {
                super::GResourcepack::NAME => self.name.borrow().to_value(),
                super::GResourcepack::PATH => self.path.borrow().to_value(),
                super::GResourcepack::TYPE => self.type_.borrow().to_value(),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }
    }
}

glib::wrapper! {
    pub struct GResourcepack(ObjectSubclass<imp::GResourcepack>);
}

impl GResourcepack {
    pub const NAME: &str = "name";
    pub const PATH: &str = "path";
    pub const TYPE: &str = "type";

    pub fn set_name(&self, value: String) {
        self.set_property(Self::NAME, value);
    }

    pub fn name(&self) -> String {
        self.property(Self::NAME)
    }

    pub fn set_path(&self, value: PathBuf) {
        self.set_property(Self::PATH, value.to_string_lossy().to_string());
    }

    pub fn path(&self) -> PathBuf {
        let value: String = self.property(Self::PATH);
        PathBuf::from(value)
    }

    pub fn set_type(&self, value: ResourcepackType) {
        let value = match value {
            ResourcepackType::Resourcepack => "resourcepack",
            ResourcepackType::Texturepack => "texturepack",
        };
        self.set_property(Self::TYPE, value);
    }

    pub fn type_(&self) -> ResourcepackType {
        let value: String = self.property(Self::TYPE);
        match value.as_str() {
            "resourcepack" => ResourcepackType::Resourcepack,
            "texturepack" => ResourcepackType::Texturepack,
            r => panic!("Invalid game type string '{r}'"),
        }
    }
}

impl From<Resourcepack> for GResourcepack {
    fn from(value: Resourcepack) -> Self {
        let object: Self = glib::Object::new(&[
            (Self::NAME, &value.name),
            (Self::PATH, &value.path.to_string_lossy().to_string()),
        ]);

        object.set_type(value._type);

        object
    }
}

impl From<GResourcepack> for Resourcepack {
    fn from(value: GResourcepack) -> Self {
        Self {
            name: value.name(),
            path: value.path(),
            _type: value.type_(),
        }
    }
}
