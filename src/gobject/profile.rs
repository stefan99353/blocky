use cobble_core::profile::CobbleProfile;
use gdk::subclass::prelude::*;
use glib::{ObjectExt, ParamFlags, ParamSpec, ParamSpecInt64, ParamSpecString, ToValue};
use once_cell::sync::{Lazy, OnceCell};
use std::cell::Cell;
use std::str::FromStr;
use time::OffsetDateTime;
use uuid::Uuid;

mod imp {
    use super::*;

    #[derive(Debug, Default)]
    pub struct GCobbleProfile {
        uuid: OnceCell<String>,
        profile_id: OnceCell<String>,
        player_name: OnceCell<String>,
        microsoft_refresh_token: OnceCell<String>,
        minecraft_token: OnceCell<String>,
        minecraft_token_exp: Cell<i64>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for GCobbleProfile {
        const NAME: &'static str = "GCobbleProfile";
        type Type = super::GCobbleProfile;
        type ParentType = glib::Object;
    }

    impl ObjectImpl for GCobbleProfile {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecString::new(
                        super::GCobbleProfile::UUID,
                        "UUID",
                        "UUID",
                        Some(&Uuid::new_v4().to_string()),
                        ParamFlags::READWRITE | ParamFlags::CONSTRUCT_ONLY,
                    ),
                    ParamSpecString::new(
                        super::GCobbleProfile::PROFILE_ID,
                        "Profile ID",
                        "Profile ID",
                        None,
                        ParamFlags::READWRITE | ParamFlags::CONSTRUCT_ONLY,
                    ),
                    ParamSpecString::new(
                        super::GCobbleProfile::PLAYER_NAME,
                        "Player Name",
                        "Player Name",
                        Some(&Uuid::new_v4().to_string()),
                        ParamFlags::READWRITE | ParamFlags::CONSTRUCT_ONLY,
                    ),
                    ParamSpecString::new(
                        super::GCobbleProfile::MICROSOFT_REFRESH_TOKEN,
                        "Microsoft Refresh Token",
                        "Microsoft Refresh Token",
                        Some(&Uuid::new_v4().to_string()),
                        ParamFlags::READWRITE | ParamFlags::CONSTRUCT_ONLY,
                    ),
                    ParamSpecString::new(
                        super::GCobbleProfile::MINECRAFT_TOKEN,
                        "Minecraft Token",
                        "Minecraft Token",
                        Some(&Uuid::new_v4().to_string()),
                        ParamFlags::READWRITE | ParamFlags::CONSTRUCT_ONLY,
                    ),
                    ParamSpecInt64::new(
                        super::GCobbleProfile::MINECRAFT_TOKEN_EXP,
                        "Minecraft Token Exp",
                        "Minecraft Token Exp",
                        i64::MIN,
                        i64::MAX,
                        OffsetDateTime::now_utc().unix_timestamp(),
                        ParamFlags::READWRITE | ParamFlags::CONSTRUCT_ONLY,
                    ),
                ]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &glib::Value, pspec: &ParamSpec) {
            match pspec.name() {
                super::GCobbleProfile::UUID => self.uuid.set(value.get().unwrap()).unwrap(),
                super::GCobbleProfile::PROFILE_ID => {
                    self.profile_id.set(value.get().unwrap()).unwrap()
                }
                super::GCobbleProfile::PLAYER_NAME => {
                    self.player_name.set(value.get().unwrap()).unwrap()
                }
                super::GCobbleProfile::MICROSOFT_REFRESH_TOKEN => self
                    .microsoft_refresh_token
                    .set(value.get().unwrap())
                    .unwrap(),
                super::GCobbleProfile::MINECRAFT_TOKEN => {
                    self.minecraft_token.set(value.get().unwrap()).unwrap()
                }
                super::GCobbleProfile::MINECRAFT_TOKEN_EXP => {
                    self.minecraft_token_exp.set(value.get().unwrap())
                }
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
            match pspec.name() {
                super::GCobbleProfile::UUID => self.uuid.get().to_value(),
                super::GCobbleProfile::PROFILE_ID => self.profile_id.get().to_value(),
                super::GCobbleProfile::PLAYER_NAME => self.player_name.get().to_value(),
                super::GCobbleProfile::MICROSOFT_REFRESH_TOKEN => {
                    self.microsoft_refresh_token.get().to_value()
                }
                super::GCobbleProfile::MINECRAFT_TOKEN => self.minecraft_token.get().to_value(),
                super::GCobbleProfile::MINECRAFT_TOKEN_EXP => {
                    self.minecraft_token_exp.get().to_value()
                }
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }
    }
}

glib::wrapper! {
    pub struct GCobbleProfile(ObjectSubclass<imp::GCobbleProfile>);
}

impl GCobbleProfile {
    pub const UUID: &str = "uuid";
    pub const PROFILE_ID: &str = "profile-id";
    pub const PLAYER_NAME: &str = "player-name";
    pub const MICROSOFT_REFRESH_TOKEN: &str = "microsoft-refresh-token";
    pub const MINECRAFT_TOKEN: &str = "minecraft-token";
    pub const MINECRAFT_TOKEN_EXP: &str = "minecraft-token-exp";

    pub fn uuid(&self) -> Uuid {
        let uuid: String = self.property(Self::UUID);
        Uuid::from_str(&uuid).unwrap()
    }

    pub fn profile_id(&self) -> String {
        self.property(Self::PROFILE_ID)
    }

    pub fn player_name(&self) -> String {
        self.property(Self::PLAYER_NAME)
    }

    pub fn microsoft_refresh_token(&self) -> String {
        self.property(Self::MICROSOFT_REFRESH_TOKEN)
    }

    pub fn minecraft_token(&self) -> String {
        self.property(Self::MINECRAFT_TOKEN)
    }

    pub fn minecraft_token_exp(&self) -> OffsetDateTime {
        let timestamp: i64 = self.property(Self::MINECRAFT_TOKEN_EXP);
        OffsetDateTime::from_unix_timestamp(timestamp).unwrap()
    }
}

impl From<CobbleProfile> for GCobbleProfile {
    fn from(value: CobbleProfile) -> Self {
        glib::Object::new(&[
            (Self::UUID, &value.uuid.to_string()),
            (Self::PROFILE_ID, &value.profile_id),
            (Self::PLAYER_NAME, &value.player_name),
            (
                Self::MICROSOFT_REFRESH_TOKEN,
                &value.microsoft_refresh_token,
            ),
            (Self::MINECRAFT_TOKEN, &value.minecraft_token),
            (
                Self::MINECRAFT_TOKEN_EXP,
                &value.minecraft_token_exp.unix_timestamp(),
            ),
        ])
    }
}

impl From<GCobbleProfile> for CobbleProfile {
    fn from(value: GCobbleProfile) -> Self {
        Self {
            uuid: value.uuid(),
            profile_id: value.profile_id(),
            player_name: value.player_name(),
            microsoft_refresh_token: value.microsoft_refresh_token(),
            minecraft_token: value.minecraft_token(),
            minecraft_token_exp: value.minecraft_token_exp(),
        }
    }
}
