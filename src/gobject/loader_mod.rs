use cobble_core::minecraft::LoaderMod;
use gdk::subclass::prelude::*;
use glib::{ObjectExt, ParamFlags, ParamSpec, ParamSpecString, ToValue};
use once_cell::sync::Lazy;
use std::cell::{Cell, RefCell};
use std::path::PathBuf;

mod imp {
    use glib::ParamSpecBoolean;

    use super::*;

    #[derive(Debug, Default)]
    pub struct GLoaderMod {
        name: RefCell<String>,
        version: RefCell<String>,
        description: RefCell<Option<String>>,
        icon_path: RefCell<Option<String>>,
        path: RefCell<String>,
        enabled: Cell<bool>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for GLoaderMod {
        const NAME: &'static str = "GLoaderMod";
        type Type = super::GLoaderMod;
        type ParentType = glib::Object;
    }

    impl ObjectImpl for GLoaderMod {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecString::new(
                        super::GLoaderMod::NAME,
                        "Name",
                        "Name",
                        None,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecString::new(
                        super::GLoaderMod::VERSION,
                        "Version",
                        "Version",
                        None,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecString::new(
                        super::GLoaderMod::DESCRIPTION,
                        "Description",
                        "Description",
                        None,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecString::new(
                        super::GLoaderMod::ICON_PATH,
                        "Icon Path",
                        "Icon Path",
                        None,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecString::new(
                        super::GLoaderMod::PATH,
                        "Path",
                        "Path",
                        None,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecBoolean::new(
                        super::GLoaderMod::ENABLED,
                        "Enabled",
                        "Enabled",
                        false,
                        ParamFlags::READWRITE,
                    ),
                ]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &glib::Value, pspec: &ParamSpec) {
            match pspec.name() {
                super::GLoaderMod::NAME => *self.name.borrow_mut() = value.get().unwrap(),
                super::GLoaderMod::VERSION => *self.version.borrow_mut() = value.get().unwrap(),
                super::GLoaderMod::DESCRIPTION => {
                    *self.description.borrow_mut() = value.get().unwrap()
                }
                super::GLoaderMod::ICON_PATH => *self.icon_path.borrow_mut() = value.get().unwrap(),
                super::GLoaderMod::PATH => *self.path.borrow_mut() = value.get().unwrap(),
                super::GLoaderMod::ENABLED => self.enabled.set(value.get().unwrap()),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
            match pspec.name() {
                super::GLoaderMod::NAME => self.name.borrow().to_value(),
                super::GLoaderMod::VERSION => self.version.borrow().to_value(),
                super::GLoaderMod::DESCRIPTION => self.description.borrow().to_value(),
                super::GLoaderMod::ICON_PATH => self.icon_path.borrow().to_value(),
                super::GLoaderMod::PATH => self.path.borrow().to_value(),
                super::GLoaderMod::ENABLED => self.enabled.get().to_value(),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }
    }
}

glib::wrapper! {
    pub struct GLoaderMod(ObjectSubclass<imp::GLoaderMod>);
}

impl GLoaderMod {
    pub const NAME: &str = "name";
    pub const VERSION: &str = "version";
    pub const DESCRIPTION: &str = "description";
    pub const ICON_PATH: &str = "icon-path";
    pub const PATH: &str = "path";
    pub const ENABLED: &str = "enabled";

    pub fn set_name(&self, value: String) {
        self.set_property(Self::NAME, value);
    }

    pub fn name(&self) -> String {
        self.property(Self::NAME)
    }

    pub fn set_version(&self, value: String) {
        self.set_property(Self::VERSION, value);
    }

    pub fn version(&self) -> String {
        self.property(Self::VERSION)
    }

    pub fn set_description(&self, value: Option<String>) {
        self.set_property(Self::DESCRIPTION, value);
    }

    pub fn description(&self) -> Option<String> {
        self.property(Self::DESCRIPTION)
    }

    pub fn set_icon_path(&self, value: Option<String>) {
        self.set_property(Self::ICON_PATH, value);
    }

    pub fn icon_path(&self) -> Option<String> {
        self.property(Self::ICON_PATH)
    }

    pub fn set_path(&self, value: PathBuf) {
        self.set_property(Self::PATH, value.to_string_lossy().to_string());
    }

    pub fn path(&self) -> PathBuf {
        let value: String = self.property(Self::PATH);
        PathBuf::from(value)
    }

    pub fn set_enabled(&self, value: bool) {
        self.set_property(Self::ENABLED, value);
    }

    pub fn enabled(&self) -> bool {
        self.property(Self::ENABLED)
    }
}

impl From<LoaderMod> for GLoaderMod {
    fn from(value: LoaderMod) -> Self {
        glib::Object::new(&[
            (Self::NAME, &value.name),
            (Self::VERSION, &value.version),
            (Self::DESCRIPTION, &value.description),
            (Self::ICON_PATH, &value.icon_path),
            (Self::PATH, &value.path.to_string_lossy().to_string()),
            (Self::ENABLED, &value.enabled),
        ])
    }
}

impl From<GLoaderMod> for LoaderMod {
    fn from(value: GLoaderMod) -> Self {
        Self {
            name: value.name(),
            version: value.version(),
            description: value.description(),
            icon_path: value.icon_path(),
            path: value.path(),
            enabled: value.enabled(),
        }
    }
}
