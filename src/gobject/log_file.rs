use cobble_core::minecraft::{LogFile, LogFileType};
use gdk::subclass::prelude::*;
use glib::{ObjectExt, ParamFlags, ParamSpec, ParamSpecInt64, ParamSpecString, ToValue};
use once_cell::sync::Lazy;
use std::cell::{Cell, RefCell};
use std::path::PathBuf;
use time::OffsetDateTime;

mod imp {
    use super::*;

    #[derive(Debug, Default)]
    pub struct GLogFile {
        name: RefCell<String>,
        path: RefCell<String>,
        type_: RefCell<String>,
        modified: Cell<i64>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for GLogFile {
        const NAME: &'static str = "GLogFile";
        type Type = super::GLogFile;
        type ParentType = glib::Object;
    }

    impl ObjectImpl for GLogFile {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecString::new(
                        super::GLogFile::NAME,
                        "Name",
                        "Name",
                        None,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecString::new(
                        super::GLogFile::PATH,
                        "Path",
                        "Path",
                        None,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecString::new(
                        super::GLogFile::TYPE,
                        "Type",
                        "Type",
                        None,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecInt64::new(
                        super::GLogFile::MODIFIED,
                        "Modified",
                        "Modified",
                        i64::MIN,
                        i64::MAX,
                        i64::MIN,
                        ParamFlags::READWRITE,
                    ),
                ]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &glib::Value, pspec: &ParamSpec) {
            match pspec.name() {
                super::GLogFile::NAME => *self.name.borrow_mut() = value.get().unwrap(),
                super::GLogFile::PATH => *self.path.borrow_mut() = value.get().unwrap(),
                super::GLogFile::TYPE => *self.type_.borrow_mut() = value.get().unwrap(),
                super::GLogFile::MODIFIED => self.modified.set(value.get().unwrap()),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
            match pspec.name() {
                super::GLogFile::NAME => self.name.borrow().to_value(),
                super::GLogFile::PATH => self.path.borrow().to_value(),
                super::GLogFile::TYPE => self.type_.borrow().to_value(),
                super::GLogFile::MODIFIED => self.modified.get().to_value(),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }
    }
}

glib::wrapper! {
    pub struct GLogFile(ObjectSubclass<imp::GLogFile>);
}

impl GLogFile {
    pub const NAME: &str = "name";
    pub const PATH: &str = "path";
    pub const TYPE: &str = "type";
    pub const MODIFIED: &str = "modified";

    pub fn set_name(&self, value: String) {
        self.set_property(Self::NAME, value);
    }

    pub fn name(&self) -> String {
        self.property(Self::NAME)
    }

    pub fn set_path(&self, value: PathBuf) {
        self.set_property(Self::PATH, value.to_string_lossy().to_string());
    }

    pub fn path(&self) -> PathBuf {
        let value: String = self.property(Self::PATH);
        PathBuf::from(value)
    }

    pub fn set_type(&self, value: LogFileType) {
        let value = match value {
            LogFileType::Plain => "plain",
            LogFileType::GzipCompressed => "gzip",
        };
        self.set_property(Self::TYPE, value);
    }

    pub fn type_(&self) -> LogFileType {
        let value: String = self.property(Self::TYPE);
        match value.as_str() {
            "plain" => LogFileType::Plain,
            "gzip" => LogFileType::GzipCompressed,
            r => panic!("Invalid log file type string '{r}'"),
        }
    }

    pub fn set_modified(&self, value: Option<OffsetDateTime>) {
        let value = value.map(|c| c.unix_timestamp()).unwrap_or(i64::MIN);
        self.set_property(Self::MODIFIED, value);
    }

    pub fn modified(&self) -> Option<OffsetDateTime> {
        let value: i64 = self.property(Self::MODIFIED);
        match value {
            i64::MIN => None,
            v => Some(OffsetDateTime::from_unix_timestamp(v).unwrap()),
        }
    }
}

impl From<LogFile> for GLogFile {
    fn from(value: LogFile) -> Self {
        let object: Self = glib::Object::new(&[
            (Self::NAME, &value.name),
            (Self::PATH, &value.path.to_string_lossy().to_string()),
            (
                Self::MODIFIED,
                &value
                    .modified
                    .map(|c| c.unix_timestamp())
                    .unwrap_or(i64::MIN),
            ),
        ]);

        object.set_type(value._type);

        object
    }
}

impl From<GLogFile> for LogFile {
    fn from(value: GLogFile) -> Self {
        Self {
            name: value.name(),
            path: value.path(),
            _type: value.type_(),
            modified: value.modified(),
        }
    }
}
