use cobble_core::minecraft::Screenshot;
use gdk::subclass::prelude::*;
use glib::{ObjectExt, ParamFlags, ParamSpec, ParamSpecInt64, ParamSpecString, ToValue};
use once_cell::sync::Lazy;
use std::cell::{Cell, RefCell};
use std::path::PathBuf;
use time::OffsetDateTime;

mod imp {
    use super::*;

    #[derive(Debug, Default)]
    pub struct GScreenshot {
        name: RefCell<String>,
        path: RefCell<String>,
        created: Cell<i64>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for GScreenshot {
        const NAME: &'static str = "GScreenshot";
        type Type = super::GScreenshot;
        type ParentType = glib::Object;
    }

    impl ObjectImpl for GScreenshot {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecString::new(
                        super::GScreenshot::NAME,
                        "Name",
                        "Name",
                        None,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecString::new(
                        super::GScreenshot::PATH,
                        "Path",
                        "Path",
                        None,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecInt64::new(
                        super::GScreenshot::CREATED,
                        "Created",
                        "Created",
                        i64::MIN,
                        i64::MAX,
                        i64::MIN,
                        ParamFlags::READWRITE,
                    ),
                ]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &glib::Value, pspec: &ParamSpec) {
            match pspec.name() {
                super::GScreenshot::NAME => *self.name.borrow_mut() = value.get().unwrap(),
                super::GScreenshot::PATH => *self.path.borrow_mut() = value.get().unwrap(),
                super::GScreenshot::CREATED => self.created.set(value.get().unwrap()),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
            match pspec.name() {
                super::GScreenshot::NAME => self.name.borrow().to_value(),
                super::GScreenshot::PATH => self.path.borrow().to_value(),
                super::GScreenshot::CREATED => self.created.get().to_value(),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }
    }
}

glib::wrapper! {
    pub struct GScreenshot(ObjectSubclass<imp::GScreenshot>);
}

impl GScreenshot {
    pub const NAME: &str = "name";
    pub const PATH: &str = "path";
    pub const CREATED: &str = "created";

    pub fn set_name(&self, value: String) {
        self.set_property(Self::NAME, value);
    }

    pub fn name(&self) -> String {
        self.property(Self::NAME)
    }

    pub fn set_path(&self, value: PathBuf) {
        self.set_property(Self::PATH, value.to_string_lossy().to_string());
    }

    pub fn path(&self) -> PathBuf {
        let value: String = self.property(Self::PATH);
        PathBuf::from(value)
    }

    pub fn set_created(&self, value: Option<OffsetDateTime>) {
        let value = value.map(|c| c.unix_timestamp()).unwrap_or(i64::MIN);
        self.set_property(Self::CREATED, value);
    }

    pub fn created(&self) -> Option<OffsetDateTime> {
        let value: i64 = self.property(Self::CREATED);
        match value {
            i64::MIN => None,
            v => Some(OffsetDateTime::from_unix_timestamp(v).unwrap()),
        }
    }
}

impl From<Screenshot> for GScreenshot {
    fn from(value: Screenshot) -> Self {
        glib::Object::new(&[
            (Self::NAME, &value.name),
            (Self::PATH, &value.path.to_string_lossy().to_string()),
            (
                Self::CREATED,
                &value
                    .created
                    .map(|c| c.unix_timestamp())
                    .unwrap_or(i64::MIN),
            ),
        ])
    }
}

impl From<GScreenshot> for Screenshot {
    fn from(value: GScreenshot) -> Self {
        Self {
            name: value.name(),
            path: value.path(),
            created: value.created(),
        }
    }
}
