use cobble_core::minecraft::Shaderpack;
use gdk::subclass::prelude::*;
use glib::{ObjectExt, ParamFlags, ParamSpec, ParamSpecString, ToValue};
use once_cell::sync::Lazy;
use std::cell::RefCell;
use std::path::PathBuf;

mod imp {
    use super::*;

    #[derive(Debug, Default)]
    pub struct GShaderpack {
        name: RefCell<String>,
        path: RefCell<String>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for GShaderpack {
        const NAME: &'static str = "GShaderpack";
        type Type = super::GShaderpack;
        type ParentType = glib::Object;
    }

    impl ObjectImpl for GShaderpack {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecString::new(
                        super::GShaderpack::NAME,
                        "Name",
                        "Name",
                        None,
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecString::new(
                        super::GShaderpack::PATH,
                        "Path",
                        "Path",
                        None,
                        ParamFlags::READWRITE,
                    ),
                ]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &glib::Value, pspec: &ParamSpec) {
            match pspec.name() {
                super::GShaderpack::NAME => *self.name.borrow_mut() = value.get().unwrap(),
                super::GShaderpack::PATH => *self.path.borrow_mut() = value.get().unwrap(),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
            match pspec.name() {
                super::GShaderpack::NAME => self.name.borrow().to_value(),
                super::GShaderpack::PATH => self.path.borrow().to_value(),
                prop => unimplemented!("Property {prop} not a member of {}", Self::NAME),
            }
        }
    }
}

glib::wrapper! {
    pub struct GShaderpack(ObjectSubclass<imp::GShaderpack>);
}

impl GShaderpack {
    pub const NAME: &str = "name";
    pub const PATH: &str = "path";

    pub fn set_name(&self, value: String) {
        self.set_property(Self::NAME, value);
    }

    pub fn name(&self) -> String {
        self.property(Self::NAME)
    }

    pub fn set_path(&self, value: PathBuf) {
        self.set_property(Self::PATH, value.to_string_lossy().to_string());
    }

    pub fn path(&self) -> PathBuf {
        let value: String = self.property(Self::PATH);
        PathBuf::from(value)
    }
}

impl From<Shaderpack> for GShaderpack {
    fn from(value: Shaderpack) -> Self {
        glib::Object::new(&[
            (Self::NAME, &value.name),
            (Self::PATH, &value.path.to_string_lossy().to_string()),
        ])
    }
}

impl From<GShaderpack> for Shaderpack {
    fn from(value: GShaderpack) -> Self {
        Self {
            name: value.name(),
            path: value.path(),
        }
    }
}
