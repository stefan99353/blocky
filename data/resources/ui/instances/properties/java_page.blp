using Gtk 4.0;
using Adw 1;

template JavaInstancePropertiesPage : Adw.PreferencesPage {
    Adw.PreferencesGroup {
        title: _("Memory");

        // Memory
        Adw.ExpanderRow override_memory_expander {
            title: _("Override Memory");
            show-enable-switch: true;
            use-underline: true;

            // Minimum
            Adw.ActionRow {
                title: _("Minimum Memory");
                focusable: false;
                activatable-widget: min_memory_spinbutton;

                Gtk.Box {
                    orientation: horizontal;
                    spacing: 6;

                    Gtk.SpinButton min_memory_spinbutton {
                        numeric: true;
                        valign: center;
                        adjustment: Gtk.Adjustment {
                            upper: 131072;
                            lower: 512;
                            step-increment: 1;
                            page-increment: 10;
                        };
                    }

                    Gtk.Label {
                        label: _("MiB");
                        valign: center;
                    }
                }
            }

            // Maximum
            Adw.ActionRow {
                title: _("Maximum Memory");
                focusable: false;
                activatable-widget: max_memory_spinbutton;

                Gtk.Box {
                    orientation: horizontal;
                    spacing: 6;

                    Gtk.SpinButton max_memory_spinbutton {
                        numeric: true;
                        valign: center;
                        adjustment: Gtk.Adjustment {
                            upper: 262144;
                            lower: 512;
                            step-increment: 1;
                            page-increment: 10;
                        };
                    }

                    Gtk.Label {
                        label: _("MiB");
                        valign: center;
                    }
                }
            }
        }
    }

    Adw.PreferencesGroup {
        title: _("Java");

        // Java Exec
        Adw.ActionRow {
            title: _("Custom Java Executable");
            subtitle: _("Leave empty to use the global Java executable");
            focusable: false;
            activatable-widget: java_exec_button;

            Gtk.Button java_exec_button {
                valign: center;
                clicked => on_select_java_exec() swapped;

                Gtk.Box {
                    spacing: 6;

                    Gtk.Image {
                        icon-name: "folder-symbolic";
                    }

                    Gtk.Label java_exec_label {
                        ellipsize: start;
                        max-width-chars: 25;
                    }
                }
            }
        }

        // JVM
        Adw.EntryRow jvm_args_entryrow {
            title: _("Custom JVM Arguments");
        }
    }
}